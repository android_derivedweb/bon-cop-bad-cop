package com.boncopbadcop.boncopbadcop.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.boncopbadcop.boncopbadcop.Model.AudioModel;
import com.boncopbadcop.boncopbadcop.Model.NewsFeedData;
import com.boncopbadcop.boncopbadcop.R;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AudioAdapterAdapter extends RecyclerView.Adapter<AudioAdapterAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private ArrayList<AudioModel> mDataset;
    private int index = -1;

    public void filterList(int pos) {
        index = pos;
        if(mDataset.get(pos).getIsPlaying()){
            mDataset.get(pos).setIsPlaying(false);
        }else {
            mDataset.get(pos).setIsPlaying(true);
        }

        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView date;
        private final ImageView playStateImage;
        private final CardView card_view_inner;


        // each data item is just a string in this case


        public MyViewHolder(View v) {
            super(v);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.playStateImage = (ImageView) itemView.findViewById(R.id.img);
            this.card_view_inner = (CardView) itemView.findViewById(R.id.card_view_inner);



        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AudioAdapterAdapter(ArrayList<AudioModel> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create a new view
        View v = layoutInflater
                .inflate(R.layout.item_audio, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.name.setText(mDataset.get(position).getName());
        Date lastModified = new Date(mDataset.get(position).getDate());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedDateString = formatter.format(lastModified);
        holder.date.setText(mDataset.get(position).getDate());
        holder.playStateImage.setImageResource(
                mDataset.get(position).isPlaying ? R.drawable.ic_pause_grey : R.drawable.ic_play_arrow_grey);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });
        if(index==position){
            holder.card_view_inner.setCardBackgroundColor(holder.card_view_inner.getContext().getResources().getColor(R.color.colorPrimary));
        }else {
            holder.playStateImage.setImageResource(R.drawable.ic_play_arrow_grey);
            holder.card_view_inner.setCardBackgroundColor(holder.card_view_inner.getContext().getResources().getColor(R.color.white));
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
