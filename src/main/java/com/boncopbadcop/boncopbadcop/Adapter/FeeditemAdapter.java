package com.boncopbadcop.boncopbadcop.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.Model.NewsFeedData;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class FeeditemAdapter extends RecyclerView.Adapter<FeeditemAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private ArrayList<NewsFeedData> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private final TextView description;
        private final TextView date;
        private final ImageView img;

        // each data item is just a string in this case


        public MyViewHolder(View v) {
            super(v);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.description = (TextView) itemView.findViewById(R.id.description);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.img = (ImageView) itemView.findViewById(R.id.img);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FeeditemAdapter(ArrayList<NewsFeedData> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create a new view
        View v = layoutInflater
                .inflate(R.layout.item_news, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.date.setText(mDataset.get(position).getDate());
        Glide.with(holder.img.getContext()).load(mDataset.get(position).getImage()).placeholder(R.drawable.apk_icon).error(R.drawable.apk_icon).into(holder.img);

        if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
            holder.title.setText(mDataset.get(position).getTitle());
            holder.description.setText(mDataset.get(position).getDescription());
        }else {
            holder.title.setText(""+mDataset.get(position).getTitleFR());
            holder.description.setText(""+mDataset.get(position).getDescriptionFR());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
