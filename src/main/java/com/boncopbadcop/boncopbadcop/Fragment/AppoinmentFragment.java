package com.boncopbadcop.boncopbadcop.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.boncopbadcop.boncopbadcop.Adapter.ApointmentAdapter;
import com.boncopbadcop.boncopbadcop.Adapter.FeeditemAdapter;
import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.ChangePasswordScreen;
import com.boncopbadcop.boncopbadcop.LoginScreen;
import com.boncopbadcop.boncopbadcop.Model.ApointmentModel;
import com.boncopbadcop.boncopbadcop.Model.NewsFeedData;
import com.boncopbadcop.boncopbadcop.NewsDetailsScreen;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class AppoinmentFragment extends Fragment {

	private static TextView m_date;
	private Locale myLocale;
	private KProgressHUD progressDialog;
	private ArrayList<ApointmentModel> mDataset = new ArrayList<>();
	private RecyclerView recyleview;
	private LinearLayoutManager linearlayout;
	private ApointmentAdapter mAdapter;


	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_appoinment, container, false);

		setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
		view.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showPopup();
			}
		});

		mDataset.clear();
		recyleview = view.findViewById(R.id.recyleview);
		recyleview.setHasFixedSize(true);
		linearlayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
		recyleview.setLayoutManager(linearlayout);
		mAdapter = new ApointmentAdapter(mDataset, new ApointmentAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {



			}
		});
		recyleview.setAdapter(mAdapter);

		GetApointment();
		return view;
	}

	private void showPopup() {
		// custom dialog
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.add_apointment);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		Window window = dialog.getWindow();
		window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

		TextView m_cancel = dialog.findViewById(R.id.m_cancel);
		m_date = dialog.findViewById(R.id.date);
		final EditText number = dialog.findViewById(R.id.number);
		LinearLayout m_cal = dialog.findViewById(R.id.ln_calender);
		m_cal.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				DialogFragment newFragment = new DatePickerFragment();
				newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

			}
		});

		dialog.findViewById(R.id.m_signup).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(m_date.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Select Date",Toast.LENGTH_SHORT).show();

				}else if(number.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Select Number",Toast.LENGTH_SHORT).show();
				}else {
					dialog.dismiss();
					AddApoinment(m_date.getText().toString(),number.getText().toString());
				}
			}
		});

		m_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}


	public static class DatePickerFragment extends DialogFragment
			implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
			dialog.getDatePicker().setMinDate(c.getTimeInMillis());
			return  dialog;
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = dateFormat.format(calendar.getTime());
			m_date.setText(dateString);
		}
	}

	public void setLocale(String lang) {

		myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
	}

	public void AddApoinment(String AppointmentDate,String AppointmentPhone){
		progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);

		progressDialog.show();
		AndroidNetworking.post(AppConstant.BASE_PATH+"add-appointment")
				.addHeaders("Accept","application/json")
				.addHeaders("Authorization","Bearer "+ PreferencesUtils.getString(AppConstant.APIToken))
				.addBodyParameter("UserID",""+PreferencesUtils.getInt(AppConstant.UserID,0))
				.addBodyParameter("AppointmentDate",AppointmentDate)
				.addBodyParameter("AppointmentPhone",AppointmentPhone)
				.setTag("appointment")
				.setPriority(Priority.MEDIUM)
				.build()
				.getAsString(new StringRequestListener() {
					@Override
					public void onResponse(String response) {

						progressDialog.dismiss();
						try {
							JSONObject jsonObject = new JSONObject(response);
							if(jsonObject.getInt("ResponseCode")==401){
								PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
								PreferencesUtils.clear();
								Intent i =  new Intent(getActivity(), LoginScreen.class);
								startActivity(i);
								getActivity().finish();
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
								return;
							}

							if (jsonObject.getInt("ResponseCode")==200) {

								mDataset.clear();
								GetApointment();
								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();


							}else {



								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();

							e.printStackTrace();
						}

					}

					@Override
					public void onError(ANError anError) {
						PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
						PreferencesUtils.clear();
						Intent i =  new Intent(getActivity(), LoginScreen.class);
						startActivity(i);
						getActivity().finish();
						progressDialog.dismiss();
						Log.e("Error",anError.getErrorBody());
						Toast.makeText(getActivity(),"Unauthenticated",Toast.LENGTH_SHORT).show();
					}
				});

	}

	public void GetApointment(){
		progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);

		progressDialog.show();
		AndroidNetworking.get(AppConstant.BASE_PATH+"get-appointment?UserID="+PreferencesUtils.getInt(AppConstant.UserID,0))
				.addHeaders("Authorization","Bearer "+ PreferencesUtils.getString(AppConstant.APIToken))
				.addHeaders("Accept","application/json")
				.setTag("appointment")
				.setPriority(Priority.MEDIUM)
				.build()
				.getAsString(new StringRequestListener() {
					@Override
					public void onResponse(String response) {

						progressDialog.dismiss();
						try {
							JSONObject jsonObject = new JSONObject(response);

							if(jsonObject.getInt("ResponseCode")==401){
								PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
								PreferencesUtils.clear();
								Intent i =  new Intent(getActivity(), LoginScreen.class);
								startActivity(i);
								getActivity().finish();
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
								return;
							}

							if (jsonObject.getInt("ResponseCode")==200) {


								JSONObject object = jsonObject.getJSONObject("data");
								JSONArray jsonArray = object.getJSONArray("data");

								for (int i = 0 ; i<jsonArray.length() ; i++){
									JSONObject jsonObject1 = jsonArray.getJSONObject(i);
									ApointmentModel newsFeedData = new ApointmentModel();
									newsFeedData.setAppointmentDate(jsonObject1.getString("AppointmentDate"));
									newsFeedData.setAppointmentPhone(jsonObject1.getString("AppointmentPhone"));
									mDataset.add(newsFeedData);
								}

								mAdapter.notifyDataSetChanged();


							}else {
								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();

							e.printStackTrace();
						}

					}

					@Override
					public void onError(ANError anError) {
						PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
						PreferencesUtils.clear();
						Intent i =  new Intent(getActivity(), LoginScreen.class);
						startActivity(i);
						getActivity().finish();
						progressDialog.dismiss();
						Log.e("Error",anError.getErrorBody());
						Toast.makeText(getActivity(),"Unauthenticated",Toast.LENGTH_SHORT).show();}
				});

	}


}