package com.boncopbadcop.boncopbadcop.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.boncopbadcop.boncopbadcop.Adapter.FeeditemAdapter;
import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.EndlessRecyclerViewScrollListener;
import com.boncopbadcop.boncopbadcop.HomeScreen;
import com.boncopbadcop.boncopbadcop.LoginInScreen;
import com.boncopbadcop.boncopbadcop.LoginScreen;
import com.boncopbadcop.boncopbadcop.Model.NewsFeedData;
import com.boncopbadcop.boncopbadcop.NewsDetailsScreen;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class NewsFragment extends Fragment {
	private Locale myLocale;
	private KProgressHUD progressDialog;
	private int last_size;
	private ArrayList<NewsFeedData> mDataset = new ArrayList<>();
	private RecyclerView recyleview;
	private LinearLayoutManager linearlayout;
	private FeeditemAdapter mAdapter;
	private  String Mpage = "1";
	private ImageView img_search;
	private EditText searchtxt;
	private boolean IsSearch = false;
	private String TEXT_SEARCH = "";
	public static InputMethodManager imm;
	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news, container, false);
		setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
		mDataset.clear();
		recyleview = view.findViewById(R.id.recyleview);
		img_search = view.findViewById(R.id.img_search);
		searchtxt = view.findViewById(R.id.searchtxt);
		recyleview.setHasFixedSize(false);
		linearlayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
		recyleview.setLayoutManager(linearlayout);
		mAdapter = new FeeditemAdapter(mDataset, new FeeditemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {

				Intent intent = new Intent(getContext(),NewsDetailsScreen.class);
				intent.putExtra("Id",mDataset.get(item).getId());
				startActivity(intent);

			}
		});
		recyleview.setAdapter(mAdapter);

		recyleview.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearlayout) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				Log.e("PageStatus",page + "  " + last_size);
				if (page!=last_size){
					Mpage = String.valueOf(page+1);
					if(IsSearch){
						GetFeedSearch(PreferencesUtils.getString(AppConstant.APIToken),Mpage,TEXT_SEARCH);
					}else {
						GetFeed(PreferencesUtils.getString(AppConstant.APIToken),Mpage);
					}




				}
			}
		});
		GetFeed(PreferencesUtils.getString(AppConstant.APIToken),Mpage);

		img_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(searchtxt.getVisibility()==View.VISIBLE){
					imm.hideSoftInputFromWindow(searchtxt.getWindowToken(), 0);
					searchtxt.setVisibility(View.GONE);
				}else {
					imm.showSoftInput(searchtxt, InputMethodManager.SHOW_FORCED);
					searchtxt.setVisibility(View.VISIBLE);
				}
			}
		});
		imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
		searchtxt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
				Mpage ="1";
				mDataset.clear();
				IsSearch = true;
				TEXT_SEARCH = charSequence.toString();
				GetFeedSearch(PreferencesUtils.getString(AppConstant.APIToken),Mpage,charSequence.toString());
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});

		searchtxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
				imm.hideSoftInputFromWindow(searchtxt.getWindowToken(), 0);
				searchtxt.setVisibility(View.GONE);
				return true;
			}
		});
		return view;
	}



	public void setLocale(String lang) {

		myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
	}

	public void GetFeed(String token,String mpage){
		progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);

		progressDialog.show();
		AndroidNetworking.get(AppConstant.BASE_PATH+"get-feed?page="+mpage)
				.addHeaders("Accept","application/json")
				.addHeaders("Authorization","Bearer "+token)
				.setTag("Feed")
				.setPriority(Priority.MEDIUM)
				.build()
				.getAsString(new StringRequestListener() {
					@Override
					public void onResponse(String response) {
						Log.e("Response : ", response);
						progressDialog.dismiss();
						try {
							JSONObject jsonObject = new JSONObject(response);
							if(jsonObject.getInt("ResponseCode")==401){
								PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
								PreferencesUtils.clear();
								Intent i =  new Intent(getActivity(), LoginScreen.class);
								startActivity(i);
								getActivity().finish();
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
								return;
							}

							if (jsonObject.getInt("ResponseCode")==200) {

								try {

									JSONObject jsonObject1 = jsonObject.getJSONObject("data");
									last_size = jsonObject1.getInt("last_page");

									JSONArray jsonArray = jsonObject1.getJSONArray("data");

									for (int i = 0 ; i<jsonArray.length() ; i++){
										JSONObject object = jsonArray.getJSONObject(i);
										NewsFeedData newsFeedData = new NewsFeedData();
										newsFeedData.setTitle(object.getString("Title"));
										newsFeedData.setTitleFR(object.getString("TitleFr"));
										newsFeedData.setDescriptionFR(object.getString("DescriptionFr"));
										newsFeedData.setId(String.valueOf(object.getInt("FeedID")));
										newsFeedData.setDescription(object.getString("Description"));
										newsFeedData.setImage(object.getString("Image"));
										newsFeedData.setDate(object.getString("Date"));
										mDataset.add(newsFeedData);
									}

									mAdapter.notifyDataSetChanged();

								} catch (JSONException e) {
									e.printStackTrace();
								}

							}else {

								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onError(ANError anError) {
						PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
						PreferencesUtils.clear();
						Intent i =  new Intent(getActivity(), LoginScreen.class);
						startActivity(i);
						getActivity().finish();
						progressDialog.dismiss();
						Log.e("Error",anError.getErrorBody());
						Toast.makeText(getActivity(),"Unauthenticated",Toast.LENGTH_SHORT).show();}
				});

	}


	public void GetFeedSearch(String token,String mpage,String search){

		AndroidNetworking.get(AppConstant.BASE_PATH+"get-feed?page="+mpage+"&searchstring="+search)
				.addHeaders("Accept","application/json")
				.addHeaders("Authorization","Bearer "+token)
				.setTag("Feed")
				.setPriority(Priority.MEDIUM)
				.build()
				.getAsString(new StringRequestListener() {
					@Override
					public void onResponse(String response) {
						mDataset.clear();
						Log.e("Response : ", response);
						try {
							JSONObject jsonObject = new JSONObject(response);
							if(jsonObject.getInt("ResponseCode")==401){
								PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
								PreferencesUtils.clear();
								Intent i =  new Intent(getActivity(), LoginScreen.class);
								startActivity(i);
								getActivity().finish();
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
								return;
							}

							if (jsonObject.getInt("ResponseCode")==200) {

								try {

									JSONObject jsonObject1 = jsonObject.getJSONObject("data");
									last_size = jsonObject1.getInt("last_page");

									JSONArray jsonArray = jsonObject1.getJSONArray("data");

									for (int i = 0 ; i<jsonArray.length() ; i++){
										JSONObject object = jsonArray.getJSONObject(i);
										NewsFeedData newsFeedData = new NewsFeedData();
										newsFeedData.setTitle(object.getString("Title"));
										newsFeedData.setTitleFR(object.getString("TitleFr"));
										newsFeedData.setDescriptionFR(object.getString("DescriptionFr"));
										newsFeedData.setId(String.valueOf(object.getInt("FeedID")));
										newsFeedData.setDescription(object.getString("Description"));
										newsFeedData.setImage(object.getString("Image"));
										newsFeedData.setDate(object.getString("Date"));
										mDataset.add(newsFeedData);
									}

									mAdapter.notifyDataSetChanged();

								} catch (JSONException e) {
									e.printStackTrace();
								}

							}else {

								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onError(ANError anError) {
						PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
						PreferencesUtils.clear();
						Intent i =  new Intent(getActivity(), LoginScreen.class);
						startActivity(i);
						getActivity().finish();
						progressDialog.dismiss();
						Log.e("Error",anError.getErrorBody());
						Toast.makeText(getActivity(),"Unauthenticated",Toast.LENGTH_SHORT).show();}
				});
	}



}