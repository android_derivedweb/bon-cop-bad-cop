package com.boncopbadcop.boncopbadcop.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;

import com.androidnetworking.interfaces.UploadProgressListener;
import com.boncopbadcop.boncopbadcop.Adapter.AudioAdapterAdapter;
import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.EndlessRecyclerViewScrollListener;
import com.boncopbadcop.boncopbadcop.HomeScreen;
import com.boncopbadcop.boncopbadcop.LoginScreen;
import com.boncopbadcop.boncopbadcop.Model.AudioModel;
import com.boncopbadcop.boncopbadcop.Model.VolleyMultipartRequest;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;
import com.boncopbadcop.boncopbadcop.RecordingActivity;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class AudioFragment extends Fragment {
	private Locale myLocale;
	// Store instance variables based on arguments passed
	private ArrayList<AudioModel> mAudioArraylist = new ArrayList<>();
	private RecyclerView recyleview;
	private LinearLayoutManager linearlayout;
	private AudioAdapterAdapter mAdapter;
	private MediaPlayer mp;
	private int CurrentPosition=-1;
	private KProgressHUD progressDialog;
	private boolean isFirsttime = false;
	private int last_size;
	private String Mpage = "1";
	private TextView no_data;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_audio, container, false);

		setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

		File folder = new File(Environment.getExternalStorageDirectory() +
				File.separator + "Audio Recorder");



		view.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				selectImage();

			}
		});

		mAudioArraylist.clear();
		/*final String path = Environment.getExternalStorageDirectory().toString()+"/BonCopBadCop";
		Log.d("Files", "Path: " + path);
		File directory = new File(path);
		File[] files = directory.listFiles();
		Log.d("Files", "Size: "+ files.length);
		for (int i = 0; i < files.length; i++)
		{
			AudioModel audioModel = new AudioModel();
			audioModel.setName(files[i].getName());
			audioModel.setDate(files[i].lastModified());
			mAudioArraylist.add(audioModel);
			Log.d("Files", "FileName:" + files[i].getName());
		}*/

		recyleview = view.findViewById(R.id.recyleview);
		no_data = view.findViewById(R.id.no_data);
		recyleview.setHasFixedSize(true);
		linearlayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
		recyleview.setLayoutManager(linearlayout);
		mAdapter = new AudioAdapterAdapter(mAudioArraylist, new AudioAdapterAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {


				if(CurrentPosition==item){
					if(mp.isPlaying()){
						mp.pause();
					}else {
						mp.start();
					}
					mAdapter.filterList(item);
				}else {
					CurrentPosition = item;
					if(mp!=null){
						mp.stop();
						mp.release();
						mp = null;
					}

					mAdapter.filterList(item);
					audioPlayer(AppConstant.BASE_PATH_RECORD,mAudioArraylist.get(item).getName());
				}



			}
		});
		recyleview.setAdapter(mAdapter);


		recyleview.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearlayout) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				Log.e("PageStatus",page + "  " + last_size);
				if (page!=last_size){
					Mpage = String.valueOf(page+1);

					GetAudio(Mpage);


				}
			}
		});

		mAdapter.notifyDataSetChanged();
		GetAudio(Mpage);
		return view;
	}


	private void selectImage() {
		try {
			PackageManager pm = getActivity().getPackageManager();
			int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
			if (hasPerm == PackageManager.PERMISSION_GRANTED) {
				final CharSequence[] options = {getString(R.string.recording), getString(R.string.pick_gallary), getString(R.string.cancel)};
				AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
				builder.setTitle(R.string.select_option);
				builder.setItems(options, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						if (options[item].equals(getResources().getString(R.string.recording))) {
							dialog.dismiss();
							Intent i = new Intent(getContext() , RecordingActivity.class);
							i.putExtra("TokenApp",PreferencesUtils.getString(AppConstant.APIToken));
							startActivity(i);
						} else if (options[item].equals(getResources().getString(R.string.pick_gallary))) {
							dialog.dismiss();
							Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent, 1);

						} else if (options[item].equals(getResources().getString(R.string.cancel))) {
							dialog.dismiss();
						}
					}
				});
				builder.show();
			} else
				checkAndroidVersion();
			//Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			checkAndroidVersion();
			//Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){

		if(requestCode == 1){

			// AddAudio(selectedVideoPath);

			Uri selectedImageUri = data.getData();
			String selectedImagePath = getRealPathFromURI(selectedImageUri);
			Log.e("selectedImagePath", selectedImagePath);
			 AddAudio(selectedImagePath);

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public String getRealPathFromURI(Uri uri) {
		if (uri == null) {
			return null;
		}
		String[] projection = {MediaStore.Audio.Media.DATA};
		Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		return uri.getPath();
	}


	private void checkAndroidVersion() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			checkAndRequestPermissions();

		} else {
			// code for lollipop and pre-lollipop devices
		}
	}
	public static final int REQUEST_ID_MULTIPLE_PERMISSIONS2 = 7;

	private boolean checkAndRequestPermissions() {
		int camera = ContextCompat.checkSelfPermission(getContext(),
				Manifest.permission.CAMERA);
		int wtite = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
		int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
		List<String> listPermissionsNeeded = new ArrayList<>();
		if (wtite != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
		}
		if (camera != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.CAMERA);
		}
		if (read != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
		}
		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS2);
			return false;
		}
		return true;
	}



	public void setLocale(String lang) {

		myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
	}

	@Override
	public void onResume() {
		super.onResume();

		/*if(isFirsttime){
			Mpage = "1";
			mAudioArraylist.clear();
			GetAudio(Mpage);
		}*/
	}

	public void audioPlayer(String path, String fileName){
		//set up MediaPlayer
		mp = new MediaPlayer();

		try {
			mp.setDataSource(path  + fileName);
			mp.prepare();
			mp.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mp != null){
			if(mp.isPlaying())
				mp.stop();

			mp.release();
			mp = null;
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mp != null){
			if(mp.isPlaying())
				mp.stop();

			mp.release();
			mp = null;
		}
	}

	public void GetAudio(String mpage){
		progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);

		progressDialog.show();
		AndroidNetworking.get(AppConstant.BASE_PATH+"get-recording?page="+mpage)
				.addHeaders("Accept","application/json")
				.addHeaders("Authorization","Bearer "+ PreferencesUtils.getString(AppConstant.APIToken))
				.setTag("Feed")
				.setPriority(Priority.MEDIUM)
				.build()
				.getAsString(new StringRequestListener() {
					@Override
					public void onResponse(String response) {
						Log.e("Response : ", response);
						isFirsttime =true;
						progressDialog.dismiss();
						try {
							JSONObject jsonObject = new JSONObject(response);
							if(jsonObject.getInt("ResponseCode")==401){
								PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
								PreferencesUtils.clear();
								Intent i =  new Intent(getActivity(), LoginScreen.class);
								startActivity(i);
								getActivity().finish();
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
								return;
							}
							if (jsonObject.getInt("ResponseCode")==200) {

								try {

									JSONObject jsonObject1 = jsonObject.getJSONObject("data");
									last_size = jsonObject1.getInt("last_page");

									JSONArray jsonArray = jsonObject1.getJSONArray("data");


									for (int i = 0; i < jsonArray.length(); i++)
									{
										JSONObject object = jsonArray.getJSONObject(i);
										AudioModel audioModel = new AudioModel();
										audioModel.setName(object.getString("FileName"));
										audioModel.setDate(object.getString("Date"));
										audioModel.setIsPlaying(false);
										mAudioArraylist.add(audioModel);

									}
									mAdapter.notifyDataSetChanged();

									mAdapter.notifyDataSetChanged();

									if(mAudioArraylist.isEmpty()){
										no_data.setVisibility(View.VISIBLE);
									}else {
										no_data.setVisibility(View.GONE);
									}

								} catch (JSONException e) {
									e.printStackTrace();
								}

							}else {

								Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onError(ANError anError) {
						PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
						PreferencesUtils.clear();
						Intent i =  new Intent(getActivity(), LoginScreen.class);
						startActivity(i);
						getActivity().finish();
						progressDialog.dismiss();
						Log.e("Error",anError.getErrorBody());
						Toast.makeText(getActivity(),"Unauthenticated",Toast.LENGTH_SHORT).show();}
				});

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		/*Mpage = "1";
		mAudioArraylist.clear();
		GetAudio(Mpage);*/

		//replaceFragment(R.id.fragment_layout,new FormsFragment(),"Fragment",null);
	}

	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag,
								   @Nullable String backStackStateName) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.addToBackStack(backStackStateName)
				.commit();
	}

	private void AddAudio(final String selectedVideoPath) {
		Log.e("Filepath",PreferencesUtils.getString(AppConstant.APIToken));
		/*final long minutes = TimeUnit.MILLISECONDS.toMinutes(file.length());

		// long seconds = (milliseconds / 1000);
		final long seconds = TimeUnit.MILLISECONDS.toSeconds(file.length());
*/
		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();
		//getting the tag from the edittex
		//our custom volley request
		VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, AppConstant.BASE_PATH+"add-recording",
				new Response.Listener<NetworkResponse>() {
					@Override
					public void onResponse(NetworkResponse response) {


						progressDialog.dismiss();
						try {
							JSONObject jsonObject = new JSONObject(new String(response.data));
							Log.e("response",jsonObject.toString());

							if (jsonObject.getInt("ResponseCode") == 200) {

								android.widget.Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();
								Intent intent = new Intent(getActivity(), HomeScreen.class);
								intent.putExtra("Verify", "1");
								startActivity(intent);
							} else {
								android.widget.Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();
							}


							Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

						} catch (Exception e) {
							Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();

						Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}) {

			/*
			 * If you want to add more parameters with the image
			 * you can do it here
			 * here we have only one parameter with the image
			 * which is tags
			 * */
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<>();
				params.put("Duration", "1:00");
				params.put("Type", "Record");
				return params;
			}


			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization", "Bearer " + PreferencesUtils.getString(AppConstant.APIToken));
				return params;
			}

			/*
			 * Here we are passing image by renaming it with a unique name
			 * */
			@Override
			protected Map<String, VolleyMultipartRequest.DataPart> getByteData() throws IOException {
				Map<String, DataPart> params = new HashMap<>();
				long imagename = System.currentTimeMillis();
				params.put("FileName", new DataPart(imagename+"" , convert(selectedVideoPath)));

				return params;
			}
		};
		//adding the request to volley
		Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
	}

	public byte[] convert(String path) throws IOException {

		FileInputStream fis = new FileInputStream(path);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] b = new byte[1024];

		for (int readNum; (readNum = fis.read(b)) != -1; ) {
			bos.write(b, 0, readNum);
		}

		byte[] bytes = bos.toByteArray();

		return bytes;
	}


}