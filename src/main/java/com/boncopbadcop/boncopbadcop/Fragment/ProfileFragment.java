package com.boncopbadcop.boncopbadcop.Fragment;

import static android.content.Context.ACTIVITY_SERVICE;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.ChangePasswordScreen;
import com.boncopbadcop.boncopbadcop.EditProfileScreen;
import com.boncopbadcop.boncopbadcop.IntroScreen;
import com.boncopbadcop.boncopbadcop.LoginScreen;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;

import java.util.Locale;


public class ProfileFragment extends Fragment {
	private Locale myLocale;
	private TextView user_name;
	private TextView email,mlanguage,number;

	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@SuppressLint("SetTextI18n")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_profile, container, false);

		setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
		view.findViewById(R.id.edit_profile).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i =  new Intent(getActivity(), EditProfileScreen.class);
				startActivity(i);
			}
		});

		view.findViewById(R.id.changepass).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i =  new Intent(getActivity(), ChangePasswordScreen.class);
				startActivity(i);
			}
		});

		view.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
				PreferencesUtils.clear();
				clearAppData();
				restartApp();
			}
		});

		user_name = view.findViewById(R.id.user_name);
		email = view.findViewById(R.id.email);
		mlanguage = view.findViewById(R.id.mlanguage);
		number = view.findViewById(R.id.number);


		email.setText(PreferencesUtils.getString(AppConstant.Email));
		if(!PreferencesUtils.getString(AppConstant.Phone).isEmpty()){
			number.setText(PreferencesUtils.getString(AppConstant.Phone));
		}

		if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
			user_name.setText("Hello,"+PreferencesUtils.getString(AppConstant.Username));
			mlanguage.setText("English");
		}else {
			user_name.setText("Bonjour, "+PreferencesUtils.getString(AppConstant.Username));
			mlanguage.setText("French");
		}



		return view;
	}

	public void setLocale(String lang) {

		myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
	}

	@Override
	public void onResume() {
		super.onResume();


		if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
			user_name.setText("Hello,"+PreferencesUtils.getString(AppConstant.Username));
			mlanguage.setText("English");
		}else {
			user_name.setText("Bonjour, "+PreferencesUtils.getString(AppConstant.Username));
			mlanguage.setText("French");
		}

		email.setText(PreferencesUtils.getString(AppConstant.Email));
		if(!PreferencesUtils.getString(AppConstant.Phone).isEmpty()){
			number.setText(PreferencesUtils.getString(AppConstant.Phone));
		}
		setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
		Log.e("Language_Code",PreferencesUtils.getString(AppConstant.LANGUAGE));


	}

	private void clearAppData() {
		try {
			// clearing app data
			if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
				((ActivityManager) getActivity().getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
			} else {
				String packageName = getActivity().getApplicationContext().getPackageName();
				Runtime runtime = Runtime.getRuntime();
				runtime.exec("pm clear " + packageName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restartApp() {
		Intent intent = new Intent(getActivity(), IntroScreen.class);
		int mPendingIntentId = 2;
		PendingIntent mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager mgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
		System.exit(0);
	}

}