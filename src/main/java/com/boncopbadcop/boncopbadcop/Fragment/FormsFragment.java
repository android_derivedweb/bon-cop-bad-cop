package com.boncopbadcop.boncopbadcop.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;

import com.boncopbadcop.boncopbadcop.AppConstant;
import com.boncopbadcop.boncopbadcop.ChangePasswordScreen;
import com.boncopbadcop.boncopbadcop.ForgotScreen;
import com.boncopbadcop.boncopbadcop.FormsDetailsScreen;
import com.boncopbadcop.boncopbadcop.LoginScreen;
import com.boncopbadcop.boncopbadcop.Model.AudioModel;
import com.boncopbadcop.boncopbadcop.NewsDetailsScreen;
import com.boncopbadcop.boncopbadcop.PreferencesUtils;
import com.boncopbadcop.boncopbadcop.R;
import com.boncopbadcop.boncopbadcop.VerifyPassowordScreen;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class FormsFragment extends Fragment {

    private TextView m_submit;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private ImageView upload_result, re_change, upload_img;

    private Locale myLocale;
    private EditText citytxt;
    private EditText mNameOfOfficer, mSerialNumber, mReasinForArrest, mNeighborhood, mPublic_specified, mPrivet_specified, mHowManyPoliceman, mFurther_info, mOrigin, mLike;
    private TextView mSubmit, mHours;

    private String Place = "";
    private String Consequence = "";
    private String mYear, mSalary, Status;
    private String Text_Arrest = "Detention";

    private String ComplaintAgaintsPolice = "0";
    private String Name = "";
    private String Phone = "";
    private String Email = "";
    private EditText txt_station, txt_car, txt_threatened, txt_insulted;
    private KProgressHUD progressDialog;
    private EditText txt_handcuffed;
    private String TEXT_HAND = "No";
    private String TEXT_Injury = "No";
    private String TEXT_Station = "No";
    private String TEXT_Car = "No";
    private String TEXT_threatened = "No";
    private String TEXT_insulted = "No";
    private Spinner mCity;
    private String mCityName;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // Inflate the view for the fragment based on layout XML
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.forms_screen_update, container, false);

        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
        Place = getString(R.string.sildewalk);
        Consequence = getString(R.string.report);
        Spinner mAge = (Spinner) view.findViewById(R.id.age);
        Spinner mIncome = (Spinner) view.findViewById(R.id.income);
        Spinner mStatus = (Spinner) view.findViewById(R.id.status);
        mCity = (Spinner) view.findViewById(R.id.city);
        upload_result = (ImageView) view.findViewById(R.id.upload_result);
        re_change = (ImageView) view.findViewById(R.id.re_change);
        upload_img = (ImageView) view.findViewById(R.id.upload_img);
        final LinearLayout arrested_layout = (LinearLayout) view.findViewById(R.id.arrested_layout);

        String[] items = new String[]{getString(R.string.t23k), getString(R.string.t40k), getString(R.string.t65k), getString(R.string.tmore)};
        String[] status = new String[]{getString(R.string.citizen), getString(R.string.permanent_resident), getString(R.string.refugee), getString(R.string.resident), getString(R.string.student), getString(R.string.claimant), getString(R.string.status), getString(R.string.autre)};
        String[] age = new String[121];

        for (int i = 0; i < 121; i++) {
            age[i] = i + "";
        }
        GetCity();
        ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(getActivity(),
                R.layout.text_view_spinner, age);
        mAge.setAdapter(adapter_age);
        mAge.setGravity(View.TEXT_ALIGNMENT_CENTER);
        mAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                mYear = (String) parent.getItemAtPosition(position);
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        ArrayAdapter<String> adapter_income = new ArrayAdapter<String>(getActivity(),
                R.layout.text_view_spinner, items);
        mIncome.setAdapter(adapter_income);
        mIncome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                mSalary = (String) parent.getItemAtPosition(position);
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(getActivity(),
                R.layout.text_view_spinner, status);
        mStatus.setAdapter(adapter_status);
        mStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                Status = (String) parent.getItemAtPosition(position);
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        citytxt = view.findViewById(R.id.citytxt);
        mNameOfOfficer = view.findViewById(R.id.name_of_officer);
        mSerialNumber = view.findViewById(R.id.serial_number);
        mReasinForArrest = view.findViewById(R.id.reason_for_arrest);
        mNeighborhood = view.findViewById(R.id.neighborhood);
        mPublic_specified = view.findViewById(R.id.public_specified);
        mPrivet_specified = view.findViewById(R.id.privet_specified);
        mHours = view.findViewById(R.id.hour);
        mHowManyPoliceman = view.findViewById(R.id.how_many_policeman);
        mFurther_info = view.findViewById(R.id.further_info);
        mOrigin = view.findViewById(R.id.origin);
        mLike = view.findViewById(R.id.like);
        mSubmit = view.findViewById(R.id.m_submit);


        RadioGroup arrested_group = (RadioGroup) view.findViewById(R.id.arrested_group);
        final RadioGroup liyu = (RadioGroup) view.findViewById(R.id.liyu);
        RadioGroup handcuffed = (RadioGroup) view.findViewById(R.id.handcuffed);
        RadioGroup injury = (RadioGroup) view.findViewById(R.id.injury);
        RadioGroup station = (RadioGroup) view.findViewById(R.id.station);
        RadioGroup car = (RadioGroup) view.findViewById(R.id.car);
        RadioGroup threatened = (RadioGroup) view.findViewById(R.id.threatened);
        RadioGroup insulted = (RadioGroup) view.findViewById(R.id.insulted);
        RadioGroup place = (RadioGroup) view.findViewById(R.id.place);
        RadioGroup consequencerd = (RadioGroup) view.findViewById(R.id.consequencerd);

        txt_handcuffed = view.findViewById(R.id.txt_handcuffed);
        txt_handcuffed.setVisibility(View.GONE);


        final LinearLayout txt_injury = view.findViewById(R.id.txt_injury);
        txt_injury.setVisibility(View.GONE);
        txt_station = view.findViewById(R.id.txt_station);
        txt_station.setVisibility(View.GONE);
        txt_car = view.findViewById(R.id.txt_car);
       final TextView othervisible = view.findViewById(R.id.othervisible);
        txt_car.setVisibility(View.GONE);
        txt_threatened = view.findViewById(R.id.txt_threatened);
        txt_threatened.setVisibility(View.GONE);
        txt_insulted = view.findViewById(R.id.txt_insulted);
        txt_insulted.setVisibility(View.GONE);
        place.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.sildewalk:
                        Place = getString(R.string.sildewalk);
                        liyu.setVisibility(View.GONE);
                        othervisible.setVisibility(View.GONE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);

                        view.findViewById(R.id.txt_public_specified).setVisibility(View.GONE);
                        mPublic_specified.setVisibility(View.GONE);
                        break;
                    case R.id.home:
                        Place = getString(R.string.home);
                        liyu.setVisibility(View.GONE);
                        othervisible.setVisibility(View.GONE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);

                        view.findViewById(R.id.txt_public_specified).setVisibility(View.GONE);
                        mPublic_specified.setVisibility(View.GONE);
                        break;
                    case R.id.carrd:
                        Place = getString(R.string.car);
                        liyu.setVisibility(View.GONE);
                        othervisible.setVisibility(View.GONE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);

                        view.findViewById(R.id.txt_public_specified).setVisibility(View.GONE);
                        mPublic_specified.setVisibility(View.GONE);
                        break;
                    case R.id.arrestedid:
                        Place = getString(R.string.Autre);
                        liyu.setVisibility(View.VISIBLE);

                        othervisible.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);

                        view.findViewById(R.id.txt_public_specified).setVisibility(View.VISIBLE);
                        mPublic_specified.setVisibility(View.VISIBLE);
                        break;

                    case R.id.others:
                        Place = "Others";
                        liyu.setVisibility(View.GONE);
                        othervisible.setVisibility(View.GONE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);

                        view.findViewById(R.id.txt_public_specified).setVisibility(View.GONE);
                        mPublic_specified.setVisibility(View.GONE);
                        showPopup22();
                        break;
                }
            }
        });


        liyu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.public2:
                        othervisible.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.txt_public_specified).setVisibility(View.VISIBLE);
                        mPublic_specified.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.GONE);
                        mPrivet_specified.setVisibility(View.GONE);
                        break;
                    case R.id.privite:
                        othervisible.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.txt_privet_specified).setVisibility(View.VISIBLE);
                        mPrivet_specified.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.txt_public_specified).setVisibility(View.GONE);
                        mPublic_specified.setVisibility(View.GONE);
                        break;
                }
            }
        });
        consequencerd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.report:
                        Consequence = getString(R.string.report);
                        break;
                    case R.id.accusation:
                        Consequence = getString(R.string.accusation);
                        break;
                    case R.id.consequence:
                        Consequence = getString(R.string.no_consequences);
                        break;
                }
            }
        });
        arrested_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.arrested:
                        Text_Arrest = "Arrest";
                        arrested_layout.setVisibility(View.VISIBLE);
                        break;
                    case R.id.detention:
                        Text_Arrest = "Detention";
                        arrested_layout.setVisibility(View.VISIBLE);
                        break;
                    case R.id.m_no:
                        Text_Arrest = "No";
                        arrested_layout.setVisibility(View.GONE);
                        break;
                }
            }
        });
        handcuffed.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.handcuffed1:
                        TEXT_HAND = "Yes";
                        txt_handcuffed.setVisibility(View.VISIBLE);
                        break;
                    case R.id.handcuffed2:
                        TEXT_HAND = "No";
                        txt_handcuffed.setVisibility(View.GONE);
                        break;
                }
            }
        });
        injury.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.injury1:
                        TEXT_Injury = "Yes";
                        txt_injury.setVisibility(View.VISIBLE);
                        break;
                    case R.id.injury2:
                        TEXT_Injury = "No";
                        txt_injury.setVisibility(View.GONE);
                        break;
                }
            }
        });
        station.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.station1:
                        TEXT_Station = "Yes";
                        txt_station.setVisibility(View.GONE);
                        break;
                    case R.id.station2:
                        TEXT_Station = "No";
                        txt_station.setVisibility(View.GONE);
                        break;
                }
            }
        });
        car.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.car1:
                        TEXT_Car = "Yes";
                        txt_car.setVisibility(View.VISIBLE);
                        break;
                    case R.id.car2:
                        TEXT_Car = "No";
                        txt_car.setVisibility(View.GONE);
                        break;
                }
            }
        });

        threatened.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.threatened1:
                        TEXT_threatened = "Yes";
                        txt_threatened.setVisibility(View.VISIBLE);
                        break;
                    case R.id.threatened2:
                        TEXT_threatened = "No";
                        txt_threatened.setVisibility(View.GONE);
                        break;
                }
            }
        });

        insulted.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.insulted1:
                        TEXT_insulted = "Yes";
                        txt_insulted.setVisibility(View.VISIBLE);
                        break;
                    case R.id.insulted2:
                        TEXT_insulted = "No";
                        txt_insulted.setVisibility(View.GONE);
                        break;
                }
            }
        });


        mHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                mHours.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }
        });

        view.findViewById(R.id.upload_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        view.findViewById(R.id.re_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        m_submit = view.findViewById(R.id.m_submit);
        m_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  if(mNameOfOfficer.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Name Of Police Officer",Toast.LENGTH_SHORT).show();
                }else if(mSerialNumber.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Serial Number",Toast.LENGTH_SHORT).show();
                }else if(mReasinForArrest.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Reason",Toast.LENGTH_SHORT).show();
                }else if(mNeighborhood.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Neighborhood",Toast.LENGTH_SHORT).show();
                }else if(mNeighborhood.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Neighborhood",Toast.LENGTH_SHORT).show();
                }else if(mPublic_specified.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Public specified",Toast.LENGTH_SHORT).show();
                }else if(mPrivet_specified.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Privet specified",Toast.LENGTH_SHORT).show();
                }else if(mHours.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Hours",Toast.LENGTH_SHORT).show();
                }else if(mOrigin.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Origin",Toast.LENGTH_SHORT).show();
                }else if(mLike.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please Enter Like",Toast.LENGTH_SHORT).show();
                }else {

                    Log.e("FormData","Name of Officer : "+mNameOfOfficer.getText().toString()+
                            "Serial Number : "+mSerialNumber.getText().toString()+
                            "Reason For Arrest : "+mReasinForArrest.getText().toString()+
                            "Neighborhood : "+mNeighborhood.getText().toString()+
                            "Place : "+Place+
                            "Public Specified : "+mPublic_specified.getText().toString()+
                            "Privet_specified : "+mPrivet_specified.getText().toString()+
                            "Hours : "+mHours.getText().toString()+
                            "Consequence : "+Consequence+
                            "ArrestOrDetention" + Text_Arrest+
                            "How Many Policeman : "+mHowManyPoliceman.getText().toString()+
                            "Handcuffed : "+txt_handcuffed.getText().toString()+
                            "Stattion : "+txt_station.getText().toString()+
                            "Car Tour : "+txt_car.getText().toString()+
                            "Threatened : "+txt_threatened.getText().toString()+
                            "Insult : "+txt_insulted.getText().toString()+
                            "Further_info : "+mFurther_info.getText().toString()+
                            "Age : "+mYear+
                            "Orign : "+mOrigin.getText().toString()+
                            "Like : "+mLike.getText().toString()+
                            "Income : "+mSalary+
                            "Canada : "+Status);


                }*/
                showPopup();

            }
        });
        checkAndroidVersion();
        return view;
    }


    private void showPopup() {
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.form_confrimation_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        TextView m_yes = dialog.findViewById(R.id.m_yes);
        m_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showPopup2();
            }
        });
        TextView m_no = dialog.findViewById(R.id.m_no);
        m_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                FromFill();
            }
        });
        dialog.show();
    }


    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.form_confrimation_dialog_2);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        final TextView name = dialog.findViewById(R.id.name);
        final TextView number = dialog.findViewById(R.id.number);
        final TextView email = dialog.findViewById(R.id.email);


        TextView m_submit = dialog.findViewById(R.id.m_submit);
        m_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Name", Toast.LENGTH_SHORT).show();
                } else if (number.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (email.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Email", Toast.LENGTH_SHORT).show();
                } else if (!email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    ComplaintAgaintsPolice = "1";
                    Name = name.getText().toString();
                    Phone = number.getText().toString();
                    Email = email.getText().toString();

                    name.setText("");
                    number.setText("");
                    email.setText("");

                    FromFill();
                }

            }
        });
        dialog.show();
    }

    // Select image from camera and gallery
    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.select_option);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals(getResources().getString(R.string.gallary))) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                checkAndroidVersion();
            //Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            checkAndroidVersion();
            //Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "BonCopBadCop2");
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "BonCopBadCop2", "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                upload_img.setVisibility(View.INVISIBLE);
                re_change.setVisibility(View.VISIBLE);
                imgPath = destination.getAbsolutePath();
                Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
                //txt_injury.setText(imgPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");

                imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());
                //  txt_injury.setText(imgPath);
                Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);

                upload_img.setVisibility(View.INVISIBLE);
                re_change.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void FromFill() {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();


        AndroidNetworking.upload(AppConstant.BASE_PATH + "add-survey")
                .addMultipartFile("ArrestProInjuryImage", destination)
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer " + PreferencesUtils.getString(AppConstant.APIToken))
                .addMultipartParameter("ArrestingPoliceOfficer", mNameOfOfficer.getText().toString())
                .addMultipartParameter("SerialNumber", mSerialNumber.getText().toString())
                .addMultipartParameter("ReasonForArrest", mReasinForArrest.getText().toString())
                .addMultipartParameter("Neighborhood", mNeighborhood.getText().toString())
                .addMultipartParameter("Place", Place)
                .addMultipartParameter("PublicSpecified", mPublic_specified.getText().toString())
                .addMultipartParameter("PrivateSpecified", mPrivet_specified.getText().toString())
                .addMultipartParameter("Hour", mHours.getText().toString())
                .addMultipartParameter("ConsequencesOfBeingStopped", Consequence)
                .addMultipartParameter("ArrestOrDetention", Text_Arrest)
                .addMultipartParameter("ArrestProHowManyPolicemen", mHowManyPoliceman.getText().toString())
                .addMultipartParameter("ArrestProHandcuffed", TEXT_HAND)
                .addMultipartParameter("ArrestProHandcuffedHowLong", txt_handcuffed.getText().toString())
                .addMultipartParameter("ArrestProInjury", TEXT_Injury)
                .addMultipartParameter("ArrestProBroughtToStation", TEXT_Station)
                .addMultipartParameter("ArrestProStationNumber", txt_station.getText().toString())
                .addMultipartParameter("ArrestProCarTour", TEXT_Car)
                .addMultipartParameter("ArrestProCarTourHowLong", txt_car.getText().toString())
                .addMultipartParameter("ArrestProHaveYouBeenThreatened", TEXT_threatened)
                .addMultipartParameter("ArrestProThreatenedSayThreats", txt_threatened.getText().toString())
                .addMultipartParameter("ArrestProWhereYouInsulted", TEXT_insulted)
                .addMultipartParameter("ArrestProWhatWereTheInsults", txt_insulted.getText().toString())
                .addMultipartParameter("ArrestProFurtherInfo", mFurther_info.getText().toString())
                .addMultipartParameter("Age", mYear)
                .addMultipartParameter("Origin", mOrigin.getText().toString())
                .addMultipartParameter("Like", mLike.getText().toString())
                .addMultipartParameter("Income", mSalary)
                .addMultipartParameter("StatusInCanada", Status)
                .addMultipartParameter("ComplaintAgaintsPolice", ComplaintAgaintsPolice)
                .addMultipartParameter("Name", Name)
                .addMultipartParameter("Phone", Phone)
                .addMultipartParameter("Email", Email)
                .addMultipartParameter("City", mCityName)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                }).getAsString(new StringRequestListener() {
            @Override
            public void onResponse(String response) {
                Log.e("Response : ", response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("ResponseCode") == 401) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin, false);
                        PreferencesUtils.clear();
                        Intent i = new Intent(getActivity(), LoginScreen.class);
                        startActivity(i);
                        getActivity().finish();
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (jsonObject.getInt("ResponseCode") == 200) {
                        mNameOfOfficer.setText("");
                        mSerialNumber.setText("");
                        mReasinForArrest.setText("");
                        mNeighborhood.setText("");
                        mPublic_specified.setText("");
                        mPrivet_specified.setText("");
                        mHours.setText("");
                        mHowManyPoliceman.setText("");
                        citytxt.setText("");
                        txt_handcuffed.setText("");
                        txt_station.setText("");
                        txt_insulted.setText("");
                        txt_car.setText("");
                        txt_threatened.setText("");
                        mFurther_info.setText("");
                        mOrigin.setText("");
                        mLike.setText("");
                        upload_result.setImageDrawable(null);
                        upload_img.setVisibility(View.VISIBLE);
                        re_change.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


// Reload current fragment
                    } else {

                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                PreferencesUtils.putBoolean(AppConstant.IsLogin, false);
                PreferencesUtils.clear();
                Intent i = new Intent(getActivity(), LoginScreen.class);
                startActivity(i);
                getActivity().finish();
                progressDialog.dismiss();
                Log.e("Error", anError.getErrorBody());
                Toast.makeText(getActivity(), "Unauthenticated", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void GetCity() {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.get(AppConstant.BASE_PATH + "get-cities")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer " + PreferencesUtils.getString(AppConstant.APIToken))
                .setTag("Feed")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("ResponseCode") == 401) {
                                PreferencesUtils.putBoolean(AppConstant.IsLogin, false);
                                PreferencesUtils.clear();
                                Intent i = new Intent(getActivity(), LoginScreen.class);
                                startActivity(i);
                                getActivity().finish();
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (jsonObject.getInt("ResponseCode") == 200) {

                                try {

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                                    String[] City = new String[jsonObject1.length()];

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject object = jsonObject1.getJSONObject(i);
                                        City[i] = object.getString("city");
                                    }


                                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(getActivity(),
                                            android.R.layout.simple_spinner_item, City);
                                    mCity.setAdapter(adapter_age);
                                    mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view,
                                                                   int position, long id) {
                                            mCityName = (String) parent.getItemAtPosition(position);
                                            Log.v("item", (String) parent.getItemAtPosition(position));
                                            Log.e("mCityName", mCityName);
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {
                                            // TODO Auto-generated method stub
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {

                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin, false);
                        PreferencesUtils.clear();
                        Intent i = new Intent(getActivity(), LoginScreen.class);
                        startActivity(i);
                        getActivity().finish();
                        progressDialog.dismiss();
                        Log.e("Error", anError.getErrorBody());
                        Toast.makeText(getActivity(), "Unauthenticated", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void showPopup22() {
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.placeothers);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        TextView m_cancel = dialog.findViewById(R.id.m_cancel);
        TextView m_signup = dialog.findViewById(R.id.m_signup);
        final EditText number = dialog.findViewById(R.id.number);


        m_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        m_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

}