package com.boncopbadcop.boncopbadcop;


import android.app.Activity;


public class PreferencesUtils {

    private Activity activity;

    // Constructor
    public PreferencesUtils(Activity activity) {
        this.activity = activity;
    }


    public static void putString(String key, String val) {
        CustomApplication.getInstance().getPreferences().edit().putString(key, val).apply();
    }

    public static void putInt(String key, int val) {
        CustomApplication.getInstance().getPreferences().edit().putInt(key, val).apply();
    }

    public static void putFloat(String key, float val) {
        CustomApplication.getInstance().getPreferences().edit().putFloat(key, val).apply();
    }

    public static void putBoolean(String key, boolean val) {
        CustomApplication.getInstance().getPreferences().edit().putBoolean(key, val).apply();
    }

    public static String getString(String key, String val) {
        return CustomApplication.getInstance().getPreferences().getString(key, val);
    }

    public static Float getFloat(String key, Float val) {
        return CustomApplication.getInstance().getPreferences().getFloat(key, val);
    }

    public static String getString(String key) {
        return CustomApplication.getInstance().getPreferences().getString(key, "");
    }

    public static int getInt(String key, int val) {
        return CustomApplication.getInstance().getPreferences().getInt(key, val);
    }

    public static boolean getBoolean(String key, boolean val) {
        return CustomApplication.getInstance().getPreferences().getBoolean(key, val);
    }

    public static boolean clear() {
        return CustomApplication.getInstance().getPreferences().edit().clear().commit();
    }
}
