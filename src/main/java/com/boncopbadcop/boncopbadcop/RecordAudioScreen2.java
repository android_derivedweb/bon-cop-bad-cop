package com.boncopbadcop.boncopbadcop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ca.uol.aig.fftpack.RealDoubleFFT;

public class RecordAudioScreen2 extends AppCompatActivity {


    private ImageView mPlayOrPause, mStop;
    private boolean IsRecordStart = false;
    public ArrayList<String> mAudioPath = new ArrayList<>();
    private KProgressHUD progressDialog;
    private File file;
    private MediaRecorder recorder;

    Boolean resume = false;
    private Chronometer cmTimer;
    long elapsedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_audio);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecordAudioScreen2.this,HomeScreen.class);
                intent.putExtra("Verify", "0");

                startActivity(intent);
            }
        });

        cmTimer = (Chronometer) findViewById(R.id.cmTimer);
        cmTimer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @SuppressLint("SetTextI18n")
            public void onChronometerTick(Chronometer arg0) {
                if (!resume) {
                    long minutes = ((SystemClock.elapsedRealtime() - cmTimer.getBase())/1000) / 60;
                    long seconds = ((SystemClock.elapsedRealtime() - cmTimer.getBase())/1000) % 60;
                    elapsedTime = SystemClock.elapsedRealtime();
                    Log.d("TAG", "onChronometerTick: " + minutes + " : " + seconds);
                    cmTimer.setText(minutes + " : " + seconds);
                } else {
                    long minutes = ((elapsedTime - cmTimer.getBase())/1000) / 60;
                    long seconds = ((elapsedTime - cmTimer.getBase())/1000) % 60;
                    elapsedTime = elapsedTime + 1000;
                    Log.d("TAG", "onChronometerTick: " + minutes + " : " + seconds);
                    cmTimer.setText(minutes +  " : " + seconds);
                }

            }
        });

        mPlayOrPause = findViewById(R.id.playpause);

        mStop = findViewById(R.id.stop);
        mStop.setVisibility(View.GONE);


        mPlayOrPause.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View view) {


                if (!IsRecordStart) {
                    if (recorder != null) {
                        recorder.reset();
                        recorder.release();
                    }
                    String mAudioName = getFilename();
                    try {
                        if (!resume) {
                            cmTimer.setBase(SystemClock.elapsedRealtime());
                            cmTimer.start();
                        } else {
                            cmTimer.start();
                        }
                        IsRecordStart = true;

                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                        recorder.setOutputFile(mAudioName);
                        recorder.prepare();
                        recorder.start();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Recording Not prepere...", Toast.LENGTH_LONG).show();
                    }

                    mAudioPath.add(mAudioName);
                    mPlayOrPause.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    mStop.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Recording Start...", Toast.LENGTH_LONG).show();

                } else {
                    cmTimer.stop();
                    resume = true;
                    IsRecordStart = false;
                    mPlayOrPause.setImageDrawable(getResources().getDrawable(R.drawable.play));
                    recorder.stop();
                    recorder.reset();   // You can reuse the object by going back to setAudioSource() step
                    recorder.release();
                    Toast.makeText(getApplicationContext(), "Recording Stop...", Toast.LENGTH_LONG).show();
                    mStop.setVisibility(View.VISIBLE);
                }

            }
        });

        mStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mergeMediaFiles(true, mAudioPath, getFilename2())) {
                    AddAudio();
                } else {
                    Toast.makeText(getApplicationContext(), "Somthing Went Wrong...", Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath + "/record_" + System.currentTimeMillis() + ".mp3");
        return (file.getAbsolutePath());
    }

    public static boolean mergeMediaFiles(boolean isAudio, ArrayList<String> sourceFiles, String targetFile) {
        try {
            String mediaKey = isAudio ? "soun" : "vide";
            List<Movie> listMovies = new ArrayList<>();
            for (String filename : sourceFiles) {
                listMovies.add(MovieCreator.build(filename));
            }
            List<Track> listTracks = new LinkedList<>();
            for (Movie movie : listMovies) {
                for (Track track : movie.getTracks()) {
                    if (track.getHandler().equals(mediaKey)) {
                        listTracks.add(track);
                    }
                }
            }
            Movie outputMovie = new Movie();
            if (!listTracks.isEmpty()) {
                outputMovie.addTrack(new AppendTrack(listTracks.toArray(new Track[listTracks.size()])));
            }
            Container container = new DefaultMp4Builder().build(outputMovie);
            FileChannel fileChannel = new RandomAccessFile(String.format(targetFile), "rws").getChannel();
            container.writeContainer(fileChannel);
            fileChannel.close();
            return true;
        } catch (IOException e) {
            Log.e("MYTAG", "Error merging media files. exception: " + e.getMessage());
            return false;
        }
    }

    public void AddAudio() {

        // long minutes = (milliseconds / 1000) / 60;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(file.length());

        // long seconds = (milliseconds / 1000);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(file.length());


        progressDialog = KProgressHUD.create(RecordAudioScreen2.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();

        AndroidNetworking.upload(AppConstant.BASE_PATH + "add-recording")
                .addMultipartFile("FileName", file)
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization", "Bearer " + PreferencesUtils.getString(AppConstant.APIToken))
                .addMultipartParameter("Duration",minutes +":"+seconds)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(RecordAudioScreen2.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(RecordAudioScreen2.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode") == 200) {

                                Toast.makeText(RecordAudioScreen2.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                finish();
                            } else {
                                Toast.makeText(RecordAudioScreen2.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(RecordAudioScreen2.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(RecordAudioScreen2.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private String getFilename2() {

        File folder = new File(RecordAudioScreen2.this.getExternalFilesDir(null) + "/" + "Audio Recorder");

        if (!folder.exists()) {
            folder.mkdirs();
        }


        file = new File(folder + "/BonCopBadCop" + System.currentTimeMillis() + ".mp3");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (file.getAbsolutePath());
    }

    @Override
    public void onBackPressed() {
        if(recorder!=null){
            ExitAlert();
        }else {
            finish();
        }
    }


    public void ExitAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure,You wanted to make decision");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        recorder.stop();
                        recorder.reset();   // You can reuse the object by going back to setAudioSource() step
                        recorder.release();

                        mergeMediaFiles(true, mAudioPath, getFilename2());
                        progressDialog = KProgressHUD.create(RecordAudioScreen2.this)
                                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                                .setLabel("Please wait")
                                .setCancellable(false)
                                .setAnimationSpeed(2)
                                .setDimAmount(0.5f);

                        progressDialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                                AddAudio();
                            }
                        }, 3000);   //5 seconds
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

/*
    public class RecordAudio extends AsyncTask<Void, double[], Void> {

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
                // int bufferSize = AudioRecord.getMinBufferSize(frequency,
                // AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                int bufferSize = AudioRecord.getMinBufferSize(frequency,
                        channelConfiguration, audioEncoding);

                recorder2 = new AudioRecord(
                        MediaRecorder.AudioSource.MIC, frequency,
                        channelConfiguration, audioEncoding, bufferSize);

                short[] buffer = new short[blockSize];
                double[] toTransform = new double[blockSize];

                recorder2.startRecording();

                // started = true; hopes this should true before calling
                // following while loop

                while (started) {
                    int bufferReadResult = recorder2.read(buffer, 0,
                            blockSize);

                    for (int i = 0; i < blockSize && i < bufferReadResult; i++) {
                        toTransform[i] = (double) buffer[i] / 32768.0; // signed
                        // 16
                    }                                       // bit
                    transformer.ft(toTransform);
                    publishProgress(toTransform);



                }

                // audioRecord.stop();

            } catch (Throwable t) {
                Toast.makeText(RecordAudioScreen2.this, "Please grant permissions to record audio", Toast.LENGTH_LONG).show();

                t.printStackTrace();
                Log.e("AudioRecord", "Recording Failed");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(double[]... toTransform) {

            canvas.drawColor(Color.WHITE);

            for (int i = 0; i < toTransform[0].length; i++) {
                int x = i;
                int downy = (int) (100 - (toTransform[0][i] * 10));
                int upy = 100;

                canvas.drawLine(x, downy, x, upy, paint);
            }

            Log.e("imageView", "imageViewimageView");
            imageView.invalidate();

            // TODO Auto-generated method stub
            // super.onProgressUpdate(values);
        }

    }
*/


}
