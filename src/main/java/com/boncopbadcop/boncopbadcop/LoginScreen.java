package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.Locale;

public class LoginScreen extends AppCompatActivity {

    private Locale myLocale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
      //  setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        findViewById(R.id.m_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginScreen.this,LoginInScreen.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.m_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginScreen.this,SignUpScreen.class);
                startActivity(intent);
            }
        });

        TextView m_login = findViewById(R.id.m_login);
        TextView m_signup = findViewById(R.id.m_signup);

        if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("fr")){
            m_login.setText("Connexion");
            m_signup.setText("Inscrivez-vous");
        }else {
            m_login.setText("Login");
            m_signup.setText("Sign Up");
        }

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Audio Recorder");
        if(!folder.exists()){
            boolean mkdirs = folder.mkdirs();
            if(mkdirs){
                Log.e("Folder","Folder Create Successfully");
            }
        }

      //  Log.e("Language",PreferencesUtils.getString(AppConstant.LANGUAGE));
        //setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

    }

    @Override
    protected void onResume() {
        super.onResume();
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
    }



    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
        if(PreferencesUtils.getBoolean(AppConstant.IsLogin,false)){
            Intent intent = new Intent(LoginScreen.this,RecordingActivityStart.class);
            intent.putExtra("TokenApp",PreferencesUtils.getString(AppConstant.APIToken));
            startActivity(intent);
            finish();
        }
    }
}