package com.boncopbadcop.boncopbadcop;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static android.os.Environment.DIRECTORY_DOWNLOADS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.WindowCallbackWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.boncopbadcop.boncopbadcop.Model.VolleyMultipartRequest;
import com.github.axet.androidlibrary.BuildConfig;
import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.animations.MarginBottomAnimation;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.services.StorageProvider;
import com.github.axet.androidlibrary.sound.AudioTrack;
import com.github.axet.androidlibrary.sound.Headset;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.PopupWindowCompat;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.audiolibrary.app.RawSamples;
import com.github.axet.audiolibrary.app.Sound;
import com.github.axet.audiolibrary.encoders.Factory;
import com.github.axet.audiolibrary.encoders.FileEncoder;
import com.github.axet.audiolibrary.encoders.FormatWAV;
import com.github.axet.audiolibrary.encoders.OnFlyEncoding;
import com.github.axet.audiolibrary.filters.AmplifierFilter;
import com.github.axet.audiolibrary.filters.SkipSilenceFilter;
import com.github.axet.audiolibrary.filters.VoiceFilter;
import com.github.axet.audiolibrary.widgets.PitchView;
import com.github.axet.audiorecorder.R;
import com.github.axet.audiorecorder.app.AudioApplication;
import com.github.axet.audiorecorder.app.Storage;
import com.github.axet.audiorecorder.services.BluetoothReceiver;
import com.github.axet.audiorecorder.services.RecordingService;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.github.axet.androidlibrary.activities.AppCompatThemeActivity.showLocked;

public class RecordingActivity extends AppCompatActivity {
    public static final String TAG = RecordingActivity.class.getSimpleName();

    public static final int RESULT_START = 1;

    public static final String[] PERMISSIONS_AUDIO = new String[]{
            Manifest.permission.RECORD_AUDIO
    };

    public static final String ERROR = RecordingActivity.class.getCanonicalName() + ".ERROR";
    public static final String START_PAUSE = RecordingActivity.class.getCanonicalName() + ".START_PAUSE";
    public static final String PAUSE_BUTTON = RecordingActivity.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static final String ACTION_FINISH_RECORDING = BuildConfig.APPLICATION_ID + ".STOP_RECORDING";

    public static final String START_RECORDING = RecordingService.class.getCanonicalName() + ".START_RECORDING";
    public static final String STOP_RECORDING = RecordingService.class.getCanonicalName() + ".STOP_RECORDING";

/*
    PhoneStateChangeListener pscl = new PhoneStateChangeListener();
*/
    FileEncoder encoder;
    Headset headset;
    Intent recordSoundIntent = null;

    boolean start = true; // do we need to start recording immidiatly?

    long editSample = -1; // current cut position in mono samples, stereo = editSample * 2

    AudioTrack play; // current play sound track

    TextView title;
    TextView time;
    String duration;
    TextView state;
    ImageView pause;
    ImageView done;
    PitchView pitch;

    AppCompatThemeActivity.ScreenReceiver screen;

    AudioApplication.RecordingStorage recording;
    ProgressEncoding pe;

    RecordingReceiver receiver;

    AlertDialog muted;
    Handler handler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == AudioApplication.RecordingStorage.PINCH)
                pitch.add((Double) msg.obj);
            if (msg.what == AudioApplication.RecordingStorage.UPDATESAMPLES)
                updateSamples((Long) msg.obj);
            if (msg.what == AudioApplication.RecordingStorage.PAUSED) {
                muted = RecordingActivity.startActivity(RecordingActivity.this, "Error", getString(R.string.mic_paused));
                if (muted != null) {
                    AutoClose ac = new AutoClose(muted, 10);
                    ac.run();
                }
            }
            /*if (msg.what == AudioApplication.RecordingStorage.MUTED) {
                if (Build.VERSION.SDK_INT >= 28)
                    muted = RecordingActivity.startActivity(RecordingActivity.this, getString(R.string.mic_muted_error), getString(R.string.mic_muted_pie));
                else
                    muted = RecordingActivity.startActivity(RecordingActivity.this, "Error", getString(R.string.mic_muted_error));
            }*/
            if (msg.what == AudioApplication.RecordingStorage.UNMUTED) {
                if (muted != null) {
                    AutoClose run = new AutoClose(muted);
                    run.run();
                    muted = null;
                }
            }
            if (msg.what == AudioApplication.RecordingStorage.END) {
                pitch.drawEnd();
                if (!recording.interrupt.get()) {
                    stopRecording(getString(R.string.recording_status_pause));
                    String text = "Error reading from stream";
                  /*  if (Build.VERSION.SDK_INT >= 28)
                        muted = RecordingActivity.startActivity(RecordingActivity.this, text, getString(R.string.mic_muted_pie));
                    else
                        muted = RecordingActivity.startActivity(RecordingActivity.this, getString(R.string.mic_muted_error), text);*/
                }
            }
            if (msg.what == AudioApplication.RecordingStorage.ERROR)
                Error((Exception) msg.obj);
        }
    };
    private String TokenApp;
    private KProgressHUD progressDialog;
    private boolean FIRSTTIME = true;

    public static void startActivity(Context context, boolean pause) {
        Log.d(TAG, "startActivity");
        Intent i = new Intent(context, RecordingActivity.class);
        if (pause)
            i.setAction(RecordingActivity.START_PAUSE);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public static AlertDialog startActivity(final Activity a, final String title, final String msg) {
        Log.d(TAG, "startActivity");
        Runnable run = new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(a, RecordingActivity.class);
                i.setAction(RecordingActivity.ERROR);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("error", title);
                i.putExtra("msg", msg);
                a.startActivity(i);
            }
        };
        if (a.isFinishing()) {
            run.run();
            return null;
        }
        try {
            AlertDialog muted = new ErrorDialog(a, msg).setTitle(title).show();
            Intent i = new Intent(a, RecordingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            a.startActivity(i);
            return muted;
        } catch (Exception e) {
            Log.d(TAG, "startActivity", e);
            run.run();
            return null;
        }
    }

    public static void stopRecording(Context context) {
        context.sendBroadcast(new Intent(ACTION_FINISH_RECORDING));
    }

    public class AutoClose implements Runnable {
        int count = 5;
        AlertDialog d;
        Button button;

        public AutoClose(AlertDialog muted, int count) {
            this(muted);
            this.count = count;
        }

        public AutoClose(AlertDialog muted) {
            d = muted;
            button = d.getButton(DialogInterface.BUTTON_NEUTRAL);
            Window w = d.getWindow();
            touchListener(w);
        }

        @SuppressWarnings("RestrictedApi")
        public void touchListener(final Window w) {
            final Window.Callback c = w.getCallback();
            w.setCallback(new WindowCallbackWrapper(c) {
                @Override
                public boolean dispatchKeyEvent(KeyEvent event) {
                    onUserInteraction();
                    return c.dispatchKeyEvent(event);
                }

                @Override
                public boolean dispatchTouchEvent(MotionEvent event) {
                    Rect rect = PopupWindowCompat.getOnScreenRect(w.getDecorView());
                    if (rect.contains((int) event.getRawX(), (int) event.getRawY()))
                        onUserInteraction();
                    return c.dispatchTouchEvent(event);
                }
            });
        }

        public void onUserInteraction() {
            Button b = d.getButton(DialogInterface.BUTTON_NEUTRAL);
            b.setVisibility(View.GONE);
            handler.removeCallbacks(this);
        }

        @Override
        public void run() {
            if (isFinishing())
                return;
            if (!d.isShowing())
                return;
            if (count <= 0) {
                d.dismiss();
                return;
            }
            button.setText(d.getContext().getString(R.string.auto_close, count));
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            count--;
            handler.postDelayed(this, 1000);
        }
    }

    class RecordingReceiver extends BluetoothReceiver {
        @Override
        public void onConnected() {
            if (recording.thread == null) {
                if (isRecordingReady())
                    startRecording();
            }
        }

        @Override
        public void onDisconnected() {
            if (recording.thread != null) {
                stopRecording(getString(R.string.hold_by_bluetooth));
                super.onDisconnected();
            }
        }

        @Override
        public void onReceive(final Context context, Intent intent) {
            super.onReceive(context, intent);
            String a = intent.getAction();
            if (a == null)
                return;
            if (a.equals(PAUSE_BUTTON)) {
                pauseButton();
                return;
            }
            if (a.equals(ACTION_FINISH_RECORDING)) {
                done.performClick();
                return;
            }
            Headset.handleIntent(headset, intent);
        }
    }

/*
    class PhoneStateChangeListener extends PhoneStateListener {
        public boolean wasRinging;
        public boolean pausedByCall;

        @Override
        public void onCallStateChanged(int s, String incomingNumber) {
            switch (s) {
                case TelephonyManager.CALL_STATE_RINGING:
                    wasRinging = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    wasRinging = true;
                    if (recording.thread != null) {
                        stopRecording(getString(R.string.hold_by_call));
                        pausedByCall = true;
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    if (pausedByCall) {
                        if (receiver.isRecordingReady())
                            startRecording();
                    }
                    wasRinging = false;
                    pausedByCall = false;
                    break;
            }
        }
    }
*/

    public static class SpeedInfo extends com.github.axet.wget.SpeedInfo {
        public Sample getLast() {
            if (samples.size() == 0)
                return null;
            return samples.get(samples.size() - 1);
        }

        public long getDuration() { // get duration of last segment [start,last]
            if (start == null || getRowSamples() < 2)
                return 0;
            return getLast().now - start.now;
        }
    }

    public static class ProgressEncoding extends ProgressDialog {
        public static int DURATION = 5000;

        public long pause;
        public long resume;
        public long samplesPause; // encoding progress on pause
        public long samplesResume; // encoding progress on resume
        SpeedInfo current;
        SpeedInfo foreground;
        SpeedInfo background;
        LinearLayout view;
        View speed;
        TextView text;
        View warning;
        RawSamples.Info info;

        public ProgressEncoding(Context context, RawSamples.Info info) {
            super(context);
            setMax(100);
            setCancelable(false);
            setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            setIndeterminate(false);
            this.info = info;
        }

        @Override
        public void setView(View v) {
            view = new LinearLayout(getContext());
            view.setOrientation(LinearLayout.VERTICAL);
            super.setView(view);
            view.addView(v);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            speed = inflater.inflate(R.layout.encoding_speed, view);
            text = (TextView) speed.findViewById(R.id.speed);
        }

        public void onPause(long cur) {
            pause = System.currentTimeMillis();
            samplesPause = cur;
            resume = 0;
            samplesResume = 0;
            if (background == null)
                background = new SpeedInfo();
            background.start(cur);
        }

        public void onResume(long cur) {
            resume = System.currentTimeMillis();
            samplesResume = cur;
            if (foreground == null)
                foreground = new SpeedInfo();
            foreground.start(cur);
        }

        public void setProgress(long cur, long total) {
            if (current == null) {
                current = new SpeedInfo();
                current.start(cur);
            } else {
                current.step(cur);
            }
            if (pause == 0 && resume == 0) { // foreground
                if (foreground == null) {
                    foreground = new SpeedInfo();
                    foreground.start(cur);
                } else {
                    foreground.step(cur);
                }
            }
            if (pause != 0 && resume == 0) // background
                background.step(cur);
            if (pause != 0 && resume != 0) { // resumed from background
                long diffreal = resume - pause; // real time
                long diffenc = (samplesResume - samplesPause) * 1000 / info.hz / info.channels; // encoding time
                if (diffreal > 0 && diffenc < diffreal && warning == null) { // paused
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    warning = inflater.inflate(R.layout.optimization, view);
                }
                if (diffreal > 0 && diffenc >= diffreal && warning == null && foreground != null && background != null) {
                    if (foreground.getDuration() > DURATION && background.getDuration() > DURATION) {
                        long r = foreground.getAverageSpeed() / background.getAverageSpeed();
                        if (r > 1) { // slowed down by twice or more
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            warning = inflater.inflate(R.layout.slow, view);
                        }
                    }
                }
            }
            text.setText(AudioApplication.formatSize(getContext(), current.getAverageSpeed() * info.bps / Byte.SIZE) + getContext().getString(R.string.per_second));
            super.setProgress((int) (cur * 100 / total));
        }
    }

    public String toMessage(Throwable e) {
        Throwable t;
        if (encoder == null) {
            t = e;
        } else {
            t = encoder.getException();
            if (t == null)
                t = e;
        }
        return ErrorDialog.toMessage(t);
    }

    public void Error(Throwable e) {
        Log.e(TAG, "error", e);
        Error(toMessage(e));
    }

    public void Error(String msg) {
        ErrorDialog builder = new ErrorDialog(this, msg);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        final File in = recording.storage.getTempRecording();
        if (in.length() > 0) {
            builder.setNeutralButton(R.string.save_as_wav, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final OpenFileDialog d = new OpenFileDialog(RecordingActivity.this, OpenFileDialog.DIALOG_TYPE.FOLDER_DIALOG);
                    d.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            OnFlyEncoding fly = new OnFlyEncoding(recording.storage, recording.storage.getNewFile(d.getCurrentPath(), FormatWAV.EXT), recording.getInfo());
                            FileEncoder encoder = new FileEncoder(RecordingActivity.this, in, fly);
                            encoding(encoder, fly, new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            });
                        }
                    });
                    d.show();
                }
            });
        }
        builder.show();
    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(RecordingActivity.this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(RecordingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(RecordingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int audio = ContextCompat.checkSelfPermission(RecordingActivity.this, Manifest.permission.RECORD_AUDIO);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (audio != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(RecordingActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS= 7;
    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(RecordingActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");



        showLocked(getWindow());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_recording);
        checkAndRequestPermissions();

        pitch = (PitchView) findViewById(R.id.recording_pitch);
        time = (TextView) findViewById(R.id.recording_time);
        state = (TextView) findViewById(R.id.recording_state);
        title = (TextView) findViewById(R.id.recording_title);

        screen = new AppCompatThemeActivity.ScreenReceiver();
        screen.registerReceiver(this);

        receiver = new RecordingReceiver();
        receiver.filter.addAction(PAUSE_BUTTON);
        receiver.filter.addAction(ACTION_FINISH_RECORDING);
        receiver.registerReceiver(this);

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        if (shared.getBoolean(AudioApplication.PREFERENCE_CALL, false)) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
           // tm.listen(pscl, PhoneStateListener.LISTEN_CALL_STATE);
        }

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                TokenApp = null;
            } else {
                TokenApp= extras.getString("TokenApp");
            }
        } else {
            TokenApp= (String) savedInstanceState.getSerializable("TokenApp");
        }

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*final View cancel = findViewById(R.id.recording_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel.setClickable(false);
                cancelDialog(new Runnable() {
                    @Override
                    public void run() {
                        stopRecording();
                        if (shared.getBoolean(AudioApplication.PREFERENCE_FLY, false)) {
                            try {
                                if (recording.e != null) {
                                    recording.e.close();
                                    recording.e = null;
                                }
                            } catch (RuntimeException e) {
                                Error(e);
                            }
                            Storage.delete(RecordingActivity.this, recording.targetUri);
                        }
                        Storage.delete(recording.storage.getTempRecording());
                        finish();
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        cancel.setClickable(true);
                    }
                });
            }
        });
*/
        done = findViewById(R.id.recording_done);
        done.setVisibility(View.GONE);
        pause = (ImageView) findViewById(R.id.recording_pause);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                done.setVisibility(View.VISIBLE);
                if(FIRSTTIME){
                    FIRSTTIME = false;
                    onCreateRecording();
                    pauseButton();
                }else {
                    pauseButton();
                }


            }
        });

        final File dir;
        if (Build.VERSION_CODES.R > Build.VERSION.SDK_INT) {
            dir = new File(Environment.getExternalStorageDirectory().getPath()
                    + "//"+getResources().getString(R.string.app_name));
        } else {
            dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath()
                    + "//"+getResources().getString(R.string.app_name));
        }

        if (!dir.exists())
            dir.mkdir();


        Log.e("DIR",dir+"");

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (encoder != null)
                    return;
                String msg;
                if (shared.getBoolean(AudioApplication.PREFERENCE_FLY, false))
                    msg = getString(R.string.recording_status_recording);
                else
                    msg = getString(R.string.recording_status_encoding);
                stopRecording(msg);
                try {
                    encoding(new Runnable() {
                        @Override
                        public void run() {
                            if (recordSoundIntent != null) {
                                recordSoundIntent.setDataAndType(StorageProvider.getProvider().share(recording.targetUri), Storage.getTypeByExt(Storage.getExt(RecordingActivity.this, recording.targetUri)));
                                FileProvider.grantPermissions(RecordingActivity.this, recordSoundIntent, FileProvider.RW);
                            }

                       /*     File folder ;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                folder = new File(RecordingActivity.this.getExternalFilesDir("BCBC") + "/" + "recordings");

                            }else {
                                folder = new File(Environment.getExternalStorageDirectory() +
                                        File.separator + "Audio Recorder");


                            }

                            Log.e("Folder",""+folder);

                            if(!folder.exists()){
                                boolean mkdirs = folder.mkdirs();
                                if(mkdirs){
                                    Log.e("Folder","Folder Create Successfully");
                                }
                            }
*/

                            File file = new File(dir+"/"+Storage.getName(RecordingActivity.this, recording.targetUri));

                            if (!file.exists()) {
                                try {
                                    file.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            Log.e("Folder",""+file);
                            UploadDatatoServer(file,TokenApp);
                        }
                    });
                } catch (RuntimeException e) {
                    Error(e);
                }
            }
        });



        Intent intent = getIntent();
        String a = intent.getAction();
        if (a != null && a.equals(START_PAUSE)) { // pretend we already start it
            start = false;
            stopRecording(getString(R.string.recording_status_pause));
        }
        onIntent(intent);
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    public void onCreateRecording() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        Intent intent = getIntent();
        String a = intent.getAction();

        AudioApplication app = AudioApplication.from(this);
        try {
            if (app.recording == null) {
                Uri targetUri = null;
                Storage storage = new Storage(this);
                if (a != null && a.equals(MediaStore.Audio.Media.RECORD_SOUND_ACTION)) {
                    if (storage.recordingPending()) {
                        String file = shared.getString(AudioApplication.PREFERENCE_TARGET, null);
                        Log.e("recordingPending1",file);
                        if (file != null) // else pending recording comes from intent recording, resume recording
                            throw new RuntimeException("finish pending recording first");
                    }
                    targetUri = storage.getNewIntentRecording();



                    recordSoundIntent = new Intent();
                } else {
                    if (storage.recordingPending()) {
                        String file = shared.getString(AudioApplication.PREFERENCE_TARGET, null);

                        Log.e("recordingPending",file);

                        if (file != null) {
                            if (file.startsWith(ContentResolver.SCHEME_CONTENT))
                                targetUri = Uri.parse(file);
                            else if (file.startsWith(ContentResolver.SCHEME_FILE))
                                targetUri = Uri.parse(file);
                            else
                                targetUri = Uri.fromFile(new File(file));
                        }
                    }
                    if (targetUri == null)
                        targetUri = storage.getNewFile();
                    SharedPreferences.Editor editor = shared.edit();
                    editor.putString(AudioApplication.PREFERENCE_TARGET, targetUri.toString());
                    editor.commit();
                }
                Log.d(TAG, "create recording at: " + targetUri);
                app.recording = new AudioApplication.RecordingStorage(this, pitch.getPitchTime(), targetUri);
            }
            recording = app.recording;
            synchronized (recording.handlers) {
                recording.handlers.add(handler);
            }
        } catch (RuntimeException e) {
            Toast.Error(this, e);
            finish();
            return;
        }
        sendBroadcast(new Intent(START_RECORDING));

        title.setText(Storage.getName(this, recording.targetUri));



        recording.updateBufferSize(false);
        edit(false, false);
        loadSamples();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onIntent(intent);
    }

    public void onIntent(Intent intent) {
        String a = intent.getAction();
        if (a != null && a.equals(ERROR))
            muted = new ErrorDialog(this, intent.getStringExtra("msg")).setTitle(intent.getStringExtra("title")).show();
    }

    void loadSamples() {
        File f = recording.storage.getTempRecording();
        if (!f.exists()) {
            recording.samplesTime = 0;
            updateSamples(recording.samplesTime);
            return;
        }

        RawSamples rs = new RawSamples(f);
        recording.samplesTime = rs.getSamples() / Sound.getChannels(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int count = pitch.getMaxPitchCount(metrics.widthPixels);

        short[] buf = new short[count * recording.samplesUpdateStereo];
        long cut = recording.samplesTime * Sound.getChannels(this) - buf.length;

        if (cut < 0)
            cut = 0;

        rs.open(cut, buf.length);
        int len = rs.read(buf);
        rs.close();

        pitch.clear(cut / recording.samplesUpdateStereo);
        int lenUpdate = len / recording.samplesUpdateStereo * recording.samplesUpdateStereo; // cut right overs (leftovers from right)
        for (int i = 0; i < lenUpdate; i += recording.samplesUpdateStereo) {
            double dB = RawSamples.getDB(buf, i, recording.samplesUpdateStereo);
            pitch.add(dB);
        }
        updateSamples(recording.samplesTime);

        int diff = len - lenUpdate;
        if (diff > 0) {
            recording.dbBuffer = ShortBuffer.allocate(recording.samplesUpdateStereo);
            recording.dbBuffer.put(buf, lenUpdate, diff);
        }
    }

    void pauseButton() {
        if (recording.thread != null) {
            receiver.errors = false;
            stopRecording(getString(R.string.recording_status_pause));
            receiver.stopBluetooth();
            headset(true, false);
        } else {
            receiver.errors = true;
            receiver.stopBluetooth(); // reset bluetooth
            editCut();
            if (receiver.isRecordingReady())
                startRecording();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if(!FIRSTTIME){
            recording.updateBufferSize(false);

            if (start) { // start once
                start = false;
                if (Storage.permitted(this, PERMISSIONS_AUDIO, RESULT_START)) { // audio perm
                    if (receiver.isRecordingReady())
                        startRecording();
                    else
                        stopRecording(getString(R.string.hold_by_bluetooth));
                }
            }

            boolean r = recording.thread != null;

            RecordingService.startService(this, Storage.getName(this, recording.targetUri), r, encoder != null, duration);

            if (r) {
                pitch.record();
            } else {
                if (editSample != -1)
                    edit(true, false);
            }

            if (pe != null)
                pe.onResume(encoder.getCurrent());
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    void stopRecording(String status) {
        setState(status);
        pause.setImageResource(R.drawable.ic_mic_24dp);
        pause.setContentDescription(getString(R.string.record_button));

        stopRecording();

        RecordingService.startService(this, Storage.getName(this, recording.targetUri), false, encoder != null, duration);

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        if (shared.getBoolean(AudioApplication.PREFERENCE_FLY, false)) {
            pitch.setOnTouchListener(null);
        } else {
            pitch.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    edit(true, true);
                    float x = event.getX();
                    if (x < 0)
                        x = 0;
                    long edit = pitch.edit(x);
                    if (edit == -1)
                        edit(false, false);
                    else
                        editSample = pitch.edit(x) * recording.samplesUpdate;
                    return true;
                }
            });
        }
    }

    void stopRecording() {
        if (recording != null) // not possible, but some devices do not call onCreate
            recording.stopRecording();
        AudioApplication.from(this).recording = null;
        handler.removeCallbacks(receiver.connected);
        pitch.stop();
        sendBroadcast(new Intent(STOP_RECORDING));
    }

    void edit(boolean show, boolean animate) {
        View box = findViewById(R.id.recording_edit_box);
        View cut = box.findViewById(R.id.recording_cut);
        final ImageView playButton = (ImageView) box.findViewById(R.id.recording_play);
        View done = box.findViewById(R.id.recording_edit_done);

        if (show) {
            setState(getString(R.string.recording_status_edit));
            editPlay(false);

            MarginBottomAnimation.apply(box, true, animate);

            cut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editCut();
                }
            });

            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (play != null) {
                        editPlay(false);
                    } else {
                        editPlay(true);
                    }
                }
            });

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edit(false, true);
                }
            });
        } else {
            editSample = -1;
            setState(getString(R.string.recording_status_pause));
            editPlay(false);
            pitch.edit(-1);
            pitch.stop();

            MarginBottomAnimation.apply(box, false, animate);
            cut.setOnClickListener(null);
            playButton.setOnClickListener(null);
            done.setOnClickListener(null);
        }
    }

    void setState(String s) {
        long free = 0;

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        int rate = Integer.parseInt(shared.getString(AudioApplication.PREFERENCE_RATE, ""));
        int m = Sound.getChannels(this);
        int c = Sound.DEFAULT_AUDIOFORMAT == AudioFormat.ENCODING_PCM_16BIT ? 2 : 1;

        long perSec;

        String ext = shared.getString(AudioApplication.PREFERENCE_ENCODING, "");

        if (shared.getBoolean(AudioApplication.PREFERENCE_FLY, false)) {
            perSec = Factory.getEncoderRate(ext, recording.sampleRate);
            try {
                free = Storage.getFree(this, recording.targetUri);
            } catch (RuntimeException e) { // IllegalArgumentException
            }
        } else { // raw file on tmp device
            perSec = c * m * rate;
            try {
                free = Storage.getFree(recording.storage.getTempRecording());
            } catch (RuntimeException e) { // IllegalArgumentException
            }
        }

        long sec = free / perSec * 1000;

        state.setText(s + "\n(" + AudioApplication.formatFree(this, free, sec) + ")");
    }

    void editPlay(boolean show) {
        View box = findViewById(R.id.recording_edit_box);
        final ImageView playButton = (ImageView) box.findViewById(R.id.recording_play);

        if (show) {
            playButton.setImageResource(R.drawable.ic_pause_black_24dp);
            playButton.setContentDescription(getString(R.string.pause_button));

            int playUpdate = PitchView.UPDATE_SPEED * recording.sampleRate / 1000;

            RawSamples rs = new RawSamples(recording.storage.getTempRecording());
            int len = (int) (rs.getSamples() - editSample * Sound.getChannels(this)); // in samples

            final AudioTrack.OnPlaybackPositionUpdateListener listener = new AudioTrack.OnPlaybackPositionUpdateListener() {
                @Override
                public void onMarkerReached(android.media.AudioTrack track) {
                    editPlay(false);
                }

                @Override
                public void onPeriodicNotification(android.media.AudioTrack track) {
                    if (play != null) {
                        long now = System.currentTimeMillis();
                        long playIndex = editSample + (now - play.playStart) * recording.sampleRate / 1000;
                        pitch.play(playIndex / (float) recording.samplesUpdate);
                    }
                }
            };

            AudioTrack.AudioBuffer buf = new AudioTrack.AudioBuffer(recording.sampleRate, Sound.getOutMode(this), Sound.DEFAULT_AUDIOFORMAT, len);
            rs.open(editSample * Sound.getChannels(this), buf.len); // len in samples
            int r = rs.read(buf.buffer); // r in samples
            if (r != buf.len)
                throw new RuntimeException("unable to read data");
            int last = buf.len / buf.getChannels() - 1;
            if (play != null)
                play.release();
            play = AudioTrack.create(Sound.SOUND_STREAM, Sound.SOUND_CHANNEL, Sound.SOUND_TYPE, buf);
            play.setNotificationMarkerPosition(last);
            play.setPositionNotificationPeriod(playUpdate);
            play.setPlaybackPositionUpdateListener(listener, handler);
            play.play();
        } else {
            if (play != null) {
                play.release();
                play = null;
            }
            pitch.play(-1);
            playButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            playButton.setContentDescription(getString(R.string.play_button));
        }
    }

    void editCut() {
        if (editSample == -1)
            return;

        RawSamples rs = new RawSamples(recording.storage.getTempRecording());
        rs.trunk((editSample + recording.samplesUpdate) * Sound.getChannels(this));
        rs.close();

        edit(false, true);
        loadSamples();
        pitch.drawCalc();
    }

    @Override
    public void onBackPressed() {
        cancelDialog(new Runnable() {
            @Override
            public void run() {

                try{
                    stopRecording();
                    Storage.delete(recording.storage.getTempRecording());
                }catch (Exception e){

                }

                finish();
            }
        }, null);
    }

    void cancelDialog(final Runnable run, final Runnable cancel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirm_cancel);
        builder.setMessage(R.string.cancel_dialog);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                run.run();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (cancel != null)
                    cancel.run();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");

        stopRecording();
        receiver.stopBluetooth();
        headset(false, false);

        if (muted != null) {
            muted.dismiss();
            muted = null;
        }

        if (screen != null) {
            screen.close();
            screen = null;
        }

        if (receiver != null) {
            receiver.close();
            receiver = null;
        }

        RecordingService.stopRecording(this);

        /*if (pscl != null) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(pscl, PhoneStateListener.LISTEN_NONE);
            pscl = null;
        }*/

        if (play != null) {
            play.release();
            play = null;
        }

        if (encoder != null) {
            encoder.close();
            encoder = null;
        }

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = shared.edit();
        editor.remove(AudioApplication.PREFERENCE_TARGET);
        editor.commit();
    }

    void startRecording() {
        try {
            edit(false, true);
            pitch.setOnTouchListener(null);

            pause.setImageResource(R.drawable.ic_pause_black_24dp);
            pause.setContentDescription(getString(R.string.pause_button));

            pitch.record();

            setState(getString(R.string.recording_status_recording));

            headset(true, true);

            recording.startRecording();

            RecordingService.startService(this, Storage.getName(this, recording.targetUri), true, encoder != null, duration);
        } catch (RuntimeException e) {
            Toast.Error(RecordingActivity.this, e);
            finish();
        }
    }

    void updateSamples(long samplesTime) {
        long ms = samplesTime / recording.sampleRate * 1000;
        duration = AudioApplication.formatDuration(this, ms);
        time.setText(duration);
        RecordingService.startService(this, Storage.getName(this, recording.targetUri), recording.thread != null, encoder != null, duration);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RESULT_START:
                if (Storage.permitted(this, permissions)) {
                    if (receiver.isRecordingReady())
                        startRecording();
                } else {
                    Toast.makeText(this, R.string.not_permitted, Toast.LENGTH_SHORT).show();
                    finish();
                }

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED&& perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                        File folder = new File(Environment.getExternalStorageDirectory() +
                                File.separator + "BonCopBadCop");
                        if (!folder.exists()) {
                            folder.mkdirs();
                        }

                        File file = new File(Environment.getExternalStorageDirectory() + "/Audio Recorder/");
                        if (!file.mkdirs()) {
                            file.mkdirs();
                        }
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(RecordingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(RecordingActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(RecordingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(RecordingActivity.this, Manifest.permission.RECORD_AUDIO)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            android.widget.Toast.makeText(RecordingActivity.this, "Go to settings and enable permissions", android.widget.Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }

        }
    }

    void encoding(final Runnable done) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(RecordingActivity.this);
        if (shared.getBoolean(AudioApplication.PREFERENCE_FLY, false)) { // keep encoder open if encoding on fly enabled
            try {
                if (recording.e != null) {
                    recording.e.close();
                    recording.e = null;
                }
            } catch (RuntimeException e) {
                Error(e);
                return;
            }
        }

        final File in = recording.storage.getTempRecording();

        final Runnable last = new Runnable() {
            @Override
            public void run() {
                SharedPreferences.Editor edit = shared.edit();
                edit.putString(AudioApplication.PREFERENCE_LAST, Storage.getName(RecordingActivity.this, recording.targetUri));
                edit.commit();
                done.run();
            }
        };

        if (!in.exists() || in.length() == 0) {
            last.run();
            return;
        }

        final OnFlyEncoding fly = new OnFlyEncoding(recording.storage, recording.targetUri, recording.getInfo());

        encoder = new FileEncoder(this, in, fly);

        if (shared.getBoolean(AudioApplication.PREFERENCE_VOICE, false))
            encoder.filters.add(new VoiceFilter(recording.getInfo()));
        float amp = shared.getFloat(AudioApplication.PREFERENCE_VOLUME, 1);
        if (amp != 1)
            encoder.filters.add(new AmplifierFilter(amp));
        if (shared.getBoolean(AudioApplication.PREFERENCE_SKIP, false))
            encoder.filters.add(new SkipSilenceFilter(recording.getInfo()));

        encoding(encoder, fly, last);
    }

    void encoding(final FileEncoder encoder, final OnFlyEncoding fly, final Runnable last) {
        RecordingService.startService(this, Storage.getName(this, fly.targetUri), recording.thread != null, encoder != null, duration);

        pe = new ProgressEncoding(this, recording.getInfo());
        pe.setTitle(R.string.encoding_title);
        pe.setMessage(".../" + Storage.getName(this, recording.targetUri));
        pe.show();

        encoder.run(new Runnable() {
            @Override
            public void run() {
                pe.setProgress(encoder.getCurrent(), encoder.getTotal());
            }
        }, new Runnable() {
            @Override
            public void run() { // success
                Storage.delete(encoder.in); // delete raw recording
                last.run();
                pe.cancel();
            }
        }, new Runnable() {
            @Override
            public void run() { // or error
                Storage.delete(RecordingActivity.this, fly.targetUri); // fly has fd, delete target manually
                pe.cancel();
                Error(encoder.getException());
            }
        });
    }

    @Override
    public void finish() {
        if (recordSoundIntent != null) {
            if (recordSoundIntent.getData() == null)
                setResult(RESULT_CANCELED);
            else
                setResult(Activity.RESULT_OK, recordSoundIntent);
            super.finish();

        } else {

            super.finish();
        }
    }

    public void headset(boolean b, final boolean recording) {
        if (b) {
            if (headset == null) {
                headset = new Headset() {
                    {
                        actions = Headset.ACTIONS_MAIN;
                    }

                    @Override
                    public void onPlay() {
                        pauseButton();
                    }

                    @Override
                    public void onPause() {
                        pauseButton();
                    }

                    @Override
                    public void onStop() {
                        pauseButton();
                    }
                };
                headset.create(this, RecordingActivity.RecordingReceiver.class);
            }
            headset.setState(recording);
        } else {
            if (headset != null) {
                headset.close();
                headset = null;
            }
        }
    }

    public void AddAudio(File file) {


        Log.e("PassData","Path : " +file.getAbsolutePath() + " " + "Token : " + TokenApp);
        // long minutes = (milliseconds / 1000) / 60;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(file.length());

        // long seconds = (milliseconds / 1000);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(file.length());


        progressDialog = KProgressHUD.create(RecordingActivity.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();

        AndroidNetworking.upload(AppConstant.BASE_PATH+"add-recording")
                .addMultipartFile("FileName", file)
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization", "Bearer " + TokenApp)
                .addMultipartParameter("Duration",minutes +":"+seconds)
                .addMultipartParameter("Type","Record")
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseAUDIO : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);


                            if (jsonObject.getInt("ResponseCode") == 200) {

                                android.widget.Toast.makeText(RecordingActivity.this, jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RecordingActivity.this, HomeScreen.class);
                                intent.putExtra("Verify", "1");
                                startActivity(intent);
                            } else {
                                android.widget.Toast.makeText(RecordingActivity.this, jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        finish();
                        android.widget.Toast.makeText(RecordingActivity.this,anError.getErrorDetail(), android.widget.Toast.LENGTH_SHORT).show();
                    }
                });

    }


    private void UploadDatatoServer(final File file , final String token) {
        Log.e("PassData", "Path : " + file.getAbsolutePath() + " " + "Token : " + token);

        // long minutes = (milliseconds / 1000) / 60;
        final long minutes = TimeUnit.MILLISECONDS.toMinutes(file.length());

        // long seconds = (milliseconds / 1000);
        final long seconds = TimeUnit.MILLISECONDS.toSeconds(file.length());


        final KProgressHUD progressDialog = KProgressHUD.create(RecordingActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, AppConstant.BASE_PATH + "add-recording",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {



                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " --");
                            if (jsonObject.getString("ResponseCode").equals("200")){
                                try {
                                    android.widget.Toast.makeText(RecordingActivity.this, jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(RecordingActivity.this, HomeScreen.class);
                                    intent.putExtra("Verify", "1");
                                    startActivity(intent);
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }else {
                                android.widget.Toast.makeText(RecordingActivity.this, jsonObject.getString("ResponseMsg"), android.widget.Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            android.widget.Toast.makeText(RecordingActivity.this, e.getMessage(), android.widget.Toast.LENGTH_SHORT).show();

                           /* session.logout();
                            Intent intent = new Intent(Activity_ManageMyWishList.this, Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            // JSONArray errors = data.getJSONArray("errors");
                            // JSONObject jsonMessage = data.getJSONObject(0);

                            Log.e("ErrorResponse",data.toString()+"");
                            String message = data.getString("message");
                            android.widget.Toast.makeText(getApplicationContext(), message, android.widget.Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                        } catch (UnsupportedEncodingException errorr) {
                        } }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Duration",  minutes + ":" + seconds);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + token);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    params.put("FileName", new DataPart(imagename + ".mp3", convertUsingTraditionalWay(file)));
                }
                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(RecordingActivity.this).add(volleyMultipartRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static byte[] convertUsingTraditionalWay(File file)
    {
        byte[] fileBytes = new byte[(int) file.length()];
        try(FileInputStream inputStream = new FileInputStream(file))
        {
            inputStream.read(fileBytes);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return fileBytes;
    }


}
