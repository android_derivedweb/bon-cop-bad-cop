package com.boncopbadcop.boncopbadcop;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.multidex.MultiDexApplication;

import com.github.axet.audiorecorder.app.AudioApplication;


/**
 * Created by mikepenz on 27.03.15.
 */
public class CustomApplication extends AudioApplication {

    private static Context context;
    private SharedPreferences preferences, preferences2;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public static Context getContext() {
        return context;
    }

    static CustomApplication app;

    public static CustomApplication getInstance() {
        if (app == null) {
            app = new CustomApplication();

        }
        app.setrPref();

        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        app = this;
    }


    public synchronized SharedPreferences getPreferences() {
        if (preferences == null)
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences;
    }

    public synchronized SharedPreferences getPermanentPreferences() {
        if (preferences2 == null)
            preferences2 = getSharedPreferences("PermanentPreferences", Context.MODE_PRIVATE);
        return preferences2;
    }

    public SharedPreferences getPref() {
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        return pref;
    }

    public SharedPreferences setrPref() {
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        return pref;
    }

}
