package com.boncopbadcop.boncopbadcop;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.Locale;

public class SelectLanguage extends AppCompatActivity {
    Locale myLocale;
    private int selectPosition=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language);


        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "BonCopBadCop");
        if (folder.isDirectory()) {
            folder.delete();
        }

        File folder2 = new File(Environment.getExternalStorageDirectory() +
                File.separator + "BonCopBadCop2");
        if (folder2.exists()) {
            folder2.delete();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        String[] items = new String[]{ getString(R.string.french),getString(R.string.english)};
        Spinner mLanguage = (Spinner) findViewById(R.id.mlanguage);
        TextView m_submit = (TextView) findViewById(R.id.m_submit);


        ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        mLanguage.setAdapter(adapter_status);
        if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
            mLanguage.setSelection(1);
        }else {
            mLanguage.setSelection(0);
        }
        mLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                selectPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        m_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Position",""+selectPosition);

                if(selectPosition==1){
                    setLocale("en");
                    PreferencesUtils.putString(AppConstant.LANGUAGE,"en");
                }else {
                    setLocale("fr");
                    PreferencesUtils.putString(AppConstant.LANGUAGE,"fr");
                }

                Intent intent = new Intent(getApplicationContext(),IntroScreen.class);
                startActivity(intent);
                finish();

            }
        });


    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}