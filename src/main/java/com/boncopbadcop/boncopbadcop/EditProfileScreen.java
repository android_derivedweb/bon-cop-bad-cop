package com.boncopbadcop.boncopbadcop;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.boncopbadcop.boncopbadcop.Model.AudioModel;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class EditProfileScreen extends AppCompatActivity {
    private Locale myLocale;
    private int selectPosition=0;
    private KProgressHUD progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_screen);

        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        String[] items = new String[]{getString(R.string.french),getString(R.string.english), };
        Spinner mLanguage = (Spinner) findViewById(R.id.mlanguage);
        final EditText User_Name = (EditText) findViewById(R.id.user_name);
        final EditText Phone_Number = (EditText) findViewById(R.id.phone_number);


        User_Name.setText(PreferencesUtils.getString(AppConstant.Username));
        Phone_Number.setText(PreferencesUtils.getString(AppConstant.Phone));
        ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        mLanguage.setAdapter(adapter_status);

        if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
            mLanguage.setSelection(1);
        }else {
            mLanguage.setSelection(0);
        }
        mLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                selectPosition = position;
                Log.e("Language_Code2",""+selectPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.m_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(User_Name.getText().toString().isEmpty()){
                    Toast.makeText(EditProfileScreen.this,"Please Enter User Name", Toast.LENGTH_SHORT).show();

                }else {
                    GetEditProfile(User_Name.getText().toString(),Phone_Number.getText().toString(),selectPosition);
                }
            }
        });


    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    public void GetEditProfile(final String UserName, final String Phone, final int LanguageID){
        progressDialog = KProgressHUD.create(EditProfileScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"update-profile")
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+ PreferencesUtils.getString(AppConstant.APIToken))
                .addBodyParameter("UserName",UserName)
                .addBodyParameter("Phone",Phone)
                .addBodyParameter("LanguageID",""+LanguageID)
                .setTag("Feed")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(EditProfileScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(EditProfileScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (jsonObject.getInt("ResponseCode") == 200) {

                                PreferencesUtils.putString(AppConstant.Username,UserName);
                                PreferencesUtils.putString(AppConstant.Phone,Phone);
                                if(LanguageID==1){
                                    PreferencesUtils.putString(AppConstant.LANGUAGE,"en");
                                }else {
                                    PreferencesUtils.putString(AppConstant.LANGUAGE,"fr");

                                }

                                Log.e("Language_Code",PreferencesUtils.getString(AppConstant.LANGUAGE));


                                Toast.makeText(EditProfileScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(EditProfileScreen.this,HomeScreen.class);
                                intent.putExtra("Verify", "0");

                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(EditProfileScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(EditProfileScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(EditProfileScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }


}