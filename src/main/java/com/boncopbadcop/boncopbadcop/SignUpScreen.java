package com.boncopbadcop.boncopbadcop;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.boncopbadcop.boncopbadcop.AppConstant.isConnectivityAvailable;

public class SignUpScreen extends AppCompatActivity {
    private Locale myLocale;
    private KProgressHUD progressDialog;
    private TextInputEditText user_name,email,password,confrim_password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String TAG = "Error";
    private int selectPosition=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_screen);
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
        AndroidNetworking.initialize(getApplicationContext());

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        TextView m_sign = findViewById(R.id.m_sign);
        String next = getString(R.string.sign_now2);
        m_sign.setText(Html.fromHtml(next));

        findViewById(R.id.m_sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpScreen.this,LoginInScreen.class);
                startActivity(intent);
            }
        });
        String[] items = new String[]{ getString(R.string.french),getString(R.string.english)};
        Spinner mLanguage = (Spinner) findViewById(R.id.mlanguage);

        user_name = findViewById(R.id.user_name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confrim_password = findViewById(R.id.confrim_password);
        ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        mLanguage.setAdapter(adapter_status);
        if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
            mLanguage.setSelection(1);
        }else {
            mLanguage.setSelection(0);
        }
        mLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                selectPosition = position;
                Log.e("Language_Code2",""+selectPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });



        findViewById(R.id.m_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isConnectivityAvailable(SignUpScreen.this)){
                    Toast.makeText(SignUpScreen.this,"Please Connect Internet",Toast.LENGTH_LONG).show();
                    return;
                }

                if(user_name.getText().toString().isEmpty()){
                    Toast.makeText(SignUpScreen.this,"Please Enter Your User Name",Toast.LENGTH_SHORT).show();
                }else  if(email.getText().toString().isEmpty()){
                    Toast.makeText(SignUpScreen.this,"Please Enter Your Email",Toast.LENGTH_SHORT).show();
                }else if (!email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }else if(password.getText().toString().isEmpty()){
                    Toast.makeText(SignUpScreen.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else if(!password.getText().toString().equals(confrim_password.getText().toString())){
                    Toast.makeText(SignUpScreen.this,"Password not match",Toast.LENGTH_SHORT).show();
                }else {
                    GetSIGNUP(user_name.getText().toString(),
                            email.getText().toString(),
                            "0",password.getText().toString());
                }
            }
        });


    }
    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    public void GetSIGNUP(String Username,String Email,String LanguageID,String Password){
        progressDialog = KProgressHUD.create(SignUpScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"register")
                .addBodyParameter("Username", Username)
                .addBodyParameter("Email", Email)
                .addBodyParameter("LanguageID", LanguageID)
                .addBodyParameter("Password", Password)
                .setTag("Register")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject  jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("ResponseCode")==200) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpScreen.this);
                                builder.setMessage(jsonObject.getString("ResponseMsg"))
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                // do LiveVideo_Recording
                                               /* final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                                                intent.setType("text/plain");
                                                final PackageManager pm = getPackageManager();
                                                final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                                                ResolveInfo best = null;
                                                for (final ResolveInfo info : matches)
                                                    if (info.activityInfo.packageName.endsWith(".gm") ||
                                                            info.activityInfo.name.toLowerCase().contains("gmail")) best = info;
                                                if (best != null)
                                                    intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                                                startActivity(intent);*/
                                                finish();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();

                            }else {
                                Toast.makeText(SignUpScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        if (error.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            // get parsed error object (If ApiError is your class)

                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                        Toast.makeText(SignUpScreen.this,"Bad Response From Server...Please after some time...",Toast.LENGTH_SHORT).show();
                    }
                });

    }


}