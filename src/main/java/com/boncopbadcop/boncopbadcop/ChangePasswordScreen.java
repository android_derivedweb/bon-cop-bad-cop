package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class ChangePasswordScreen extends AppCompatActivity {
    private Locale myLocale;
    private KProgressHUD progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass_screen);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        final EditText Old_Password = findViewById(R.id.old_password);
        final EditText New_Password = findViewById(R.id.new_password);
        final EditText Password = findViewById(R.id.password);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.m_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Old_Password.getText().toString().isEmpty()){
                    Toast.makeText(ChangePasswordScreen.this, "Please Enter Old Password", Toast.LENGTH_SHORT).show();

                }else if(Password.getText().toString().isEmpty()){
                    Toast.makeText(ChangePasswordScreen.this, "Please Enter Password", Toast.LENGTH_SHORT).show();

                }else if(New_Password.getText().toString().isEmpty()){
                    Toast.makeText(ChangePasswordScreen.this, "Please Enter New Password", Toast.LENGTH_SHORT).show();

                }else if(!Password.getText().toString().equals(New_Password.getText().toString())){
                    Toast.makeText(ChangePasswordScreen.this,"Password not match",Toast.LENGTH_SHORT).show();
                }else {
                    GetChangePassword(Old_Password.getText().toString(),Password.getText().toString(),New_Password.getText().toString());
                }
            }
        });
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    public void GetChangePassword(final String Old, final String Pass, String New){
        progressDialog = KProgressHUD.create(ChangePasswordScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"change-password")
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+ PreferencesUtils.getString(AppConstant.APIToken))
                .addBodyParameter("OldPassword",Old)
                .addBodyParameter("NewPassword",Pass)
                .addBodyParameter("ConfirmPassword",""+New)
                .setTag("Feed")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(ChangePasswordScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(ChangePasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (jsonObject.getInt("ResponseCode") == 200) {


                                Toast.makeText(ChangePasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                finish();
                            } else {

                                Toast.makeText(ChangePasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(ChangePasswordScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(ChangePasswordScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show(); }
                });

    }


}