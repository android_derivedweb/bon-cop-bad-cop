package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.boncopbadcop.boncopbadcop.AppConstant.isConnectivityAvailable;

public class ResetPasswordScreen extends AppCompatActivity {
    private Locale myLocale;
    private KProgressHUD progressDialog;
    private TextInputEditText password,confrim_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_screen);
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        AndroidNetworking.initialize(getApplicationContext());


        final String Email;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                Email= null;
            } else {
                Email= extras.getString("Email");
            }
        } else {
            Email= (String) savedInstanceState.getSerializable("Email");
        }
        password = findViewById(R.id.password);
        confrim_password = findViewById(R.id.confrim_password);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.m_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isConnectivityAvailable(ResetPasswordScreen.this)){
                    Toast.makeText(ResetPasswordScreen.this,"Please Connect Internet",Toast.LENGTH_LONG).show();
                    return;
                }

                if(password.getText().toString().isEmpty()){
                    Toast.makeText(ResetPasswordScreen.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else if(confrim_password.getText().toString().isEmpty()){
                    Toast.makeText(ResetPasswordScreen.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else if(!password.getText().toString().equals(confrim_password.getText().toString())){
                    Toast.makeText(ResetPasswordScreen.this,"Password not match",Toast.LENGTH_SHORT).show();
                }else {
                    GetNewPassword(Email,password.getText().toString());
                }
            }
        });
    }

    public void GetNewPassword(final String Email, String Password){
        progressDialog = KProgressHUD.create(ResetPasswordScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"new-password")
                .addBodyParameter("Email", Email)
                .addBodyParameter("NewPassword", Password)
                .addBodyParameter("ConfirmPassword", Password)
                .setTag("NewPassoword")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(ResetPasswordScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(ResetPasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {
                                Intent intent = new Intent(ResetPasswordScreen.this,LoginInScreen.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(ResetPasswordScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(ResetPasswordScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(ResetPasswordScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show(); }
                });

    }


    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}