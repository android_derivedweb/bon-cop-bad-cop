package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.boncopbadcop.boncopbadcop.AppConstant.isConnectivityAvailable;

public class ForgotScreen extends AppCompatActivity {
    private Locale myLocale;
    private TextInputEditText email;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private KProgressHUD progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_screen);
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        AndroidNetworking.initialize(getApplicationContext());

        email = findViewById(R.id.email);
        findViewById(R.id.m_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isConnectivityAvailable(ForgotScreen.this)){
                    Toast.makeText(ForgotScreen.this,"Please Connect Internet",Toast.LENGTH_LONG).show();
                    return;
                }

                if(email.getText().toString().isEmpty()){
                    Toast.makeText(ForgotScreen.this,"Please Enter Your Email",Toast.LENGTH_SHORT).show();
                }else if (!email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }else {
                    GetForgot(email.getText().toString());
                }

            }
        });
    }
    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void GetForgot(final String Email){
        progressDialog = KProgressHUD.create(ForgotScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"forgot-password")
                .addBodyParameter("Email", Email)
                .setTag("Forgot")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(ForgotScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(ForgotScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {
                                Toast.makeText(ForgotScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ForgotScreen.this,VerifyPassowordScreen.class);
                                intent.putExtra("Email",Email);
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(ForgotScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(ForgotScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(ForgotScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }

}