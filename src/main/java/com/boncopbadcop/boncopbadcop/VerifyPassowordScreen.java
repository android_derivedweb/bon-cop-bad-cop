package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.boncopbadcop.boncopbadcop.AppConstant.isConnectivityAvailable;

public class VerifyPassowordScreen extends AppCompatActivity {

    private Locale myLocale;
    private EditText et1,et2,et3,et4;
    private KProgressHUD progressDialog;
    private String OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_screen);
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        AndroidNetworking.initialize(getApplicationContext());

        final String Email;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                Email= null;
            } else {
                Email= extras.getString("Email");
            }
        } else {
            Email= (String) savedInstanceState.getSerializable("Email");
        }

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        et1 = findViewById(R.id.editText1);
        et2 = findViewById(R.id.editText2);
        et3 = findViewById(R.id.editText3);
        et4 = findViewById(R.id.editText4);

        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));

        findViewById(R.id.m_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isConnectivityAvailable(VerifyPassowordScreen.this)){
                    Toast.makeText(VerifyPassowordScreen.this,"Please Connect Internet",Toast.LENGTH_LONG).show();
                    return;
                }

                if(et1.getText().toString().isEmpty()){
                    Toast.makeText(VerifyPassowordScreen.this,"Please enter OTP",Toast.LENGTH_SHORT).show();

                }else if(et2.getText().toString().isEmpty()){
                    Toast.makeText(VerifyPassowordScreen.this,"Please enter OTP",Toast.LENGTH_SHORT).show();

                }else if(et3.getText().toString().isEmpty()){
                    Toast.makeText(VerifyPassowordScreen.this,"Please enter OTP",Toast.LENGTH_SHORT).show();

                }else if(et4.getText().toString().isEmpty()){
                    Toast.makeText(VerifyPassowordScreen.this,"Please enter OTP",Toast.LENGTH_SHORT).show();

                }else {
                    OTP = et1.getText().toString()+
                            et2.getText().toString()+
                            et3.getText().toString()+
                            et4.getText().toString();

                    GetVerification(Email,OTP);
                }


            }
        });
    }
    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.editText1:
                    if(text.length()==1)
                        et2.requestFocus();
                    break;
                case R.id.editText2:
                    if(text.length()==1)
                        et3.requestFocus();
                    else if(text.length()==0)
                        et1.requestFocus();
                    break;
                case R.id.editText3:
                    if(text.length()==1)
                        et4.requestFocus();
                    else if(text.length()==0)
                        et2.requestFocus();
                    break;
                case R.id.editText4:
                    if(text.length()==0)
                        et3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    public void GetVerification(final String Email, String Otp){
        progressDialog = KProgressHUD.create(VerifyPassowordScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"check-otp")
                .addBodyParameter("Email", Email)
                .addBodyParameter("OTP", Otp)
                .setTag("OTP")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(VerifyPassowordScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(VerifyPassowordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {

                                Intent intent = new Intent(VerifyPassowordScreen.this,ResetPasswordScreen.class);
                                intent.putExtra("Email",Email);
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(VerifyPassowordScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(VerifyPassowordScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(VerifyPassowordScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }


}