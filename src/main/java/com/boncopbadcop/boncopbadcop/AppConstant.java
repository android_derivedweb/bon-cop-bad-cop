package com.boncopbadcop.boncopbadcop;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppConstant {

    public static final String BASE_PATH = "https://boncopbadcop.com/api/";
   // public static final String BASE_PATH = "https://boncopbadcop.com/backend/api/";
    public static final String BASE_PATH_RECORD = "https://boncopbadcop.com/public/recording/";
    public static String LANGUAGE = "LANGUAGE";
    public static String IsLogin = "Login";
    public static String UserID = "UserID";
    public static String Username = "Username";
    public static String Email = "Email";
    public static String Phone = "Phone";
    public static String APIToken = "APIToken";

    public static boolean isConnectivityAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

}
