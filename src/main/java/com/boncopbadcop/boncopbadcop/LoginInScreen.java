package com.boncopbadcop.boncopbadcop;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.PublicKey;
import java.util.Locale;

import static com.boncopbadcop.boncopbadcop.AppConstant.isConnectivityAvailable;

public class LoginInScreen extends AppCompatActivity {

    private Locale myLocale;
    private KProgressHUD progressDialog;
    private TextInputEditText user_name,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_in_screen);

        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        TextView m_sign = findViewById(R.id.m_sign);
        user_name = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        String first = getString(R.string.dont_have_an_account);
        String next = "<font color='#0285fb'>"+getString(R.string.signup_now)+"</font>";
        m_sign.setText(Html.fromHtml(first + next));

        AndroidNetworking.initialize(getApplicationContext());


        findViewById(R.id.m_forgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginInScreen.this,ForgotScreen.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.m_sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginInScreen.this,SignUpScreen.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.m_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!isConnectivityAvailable(LoginInScreen.this)){
                    Toast.makeText(LoginInScreen.this, R.string.internet,Toast.LENGTH_LONG).show();
                    return;
                }
                if(user_name.getText().toString().isEmpty()){
                    Toast.makeText(LoginInScreen.this, R.string.email_username,Toast.LENGTH_SHORT).show();
                }else if(password.getText().toString().isEmpty()){
                    Toast.makeText(LoginInScreen.this, R.string.enter_password,Toast.LENGTH_SHORT).show();
                }else {
                    GetLogin(user_name.getText().toString(),password.getText().toString());
                }

            }
        });

    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void GetLogin(String Email,String Password){
        progressDialog = KProgressHUD.create(LoginInScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"login")
                .addBodyParameter("Email", Email)
                .addBodyParameter("Password", Password)
                .setTag("Login")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject  jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("ResponseCode")==200) {
                                if (jsonObject.getJSONObject("data").getInt("LanguageID") == 1) {
                                    PreferencesUtils.putString(AppConstant.LANGUAGE, "en");
                                } else {
                                    PreferencesUtils.putString(AppConstant.LANGUAGE, "fr");

                                }
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,true);
                                PreferencesUtils.putInt(AppConstant.UserID,jsonObject.getJSONObject("data").getInt("UserID"));
                                PreferencesUtils.putString(AppConstant.Username,jsonObject.getJSONObject("data").getString("Username"));
                                PreferencesUtils.putString(AppConstant.Email,jsonObject.getJSONObject("data").getString("Email"));
                                PreferencesUtils.putString(AppConstant.APIToken,jsonObject.getJSONObject("data").getString("APIToken"));
                                Intent intent = new Intent(LoginInScreen.this,HomeScreen.class);
                                intent.putExtra("Verify", "0");
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(LoginInScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(LoginInScreen.this,"Bad Response From Server...Please after some time...",Toast.LENGTH_SHORT).show();
                    }
                });

    }


}