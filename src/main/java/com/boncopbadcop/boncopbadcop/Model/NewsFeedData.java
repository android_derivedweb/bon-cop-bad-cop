package com.boncopbadcop.boncopbadcop.Model;

public class NewsFeedData {
    private String title;
    private String id;
    private String description;
    private String image;
    private String date;
    private String titleFR;
    private String descriptionFR;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTitleFR(String titleFR) {
        this.titleFR = titleFR;
    }

    public String getTitleFR() {
        return titleFR;
    }

    public void setDescriptionFR(String descriptionFR) {
        this.descriptionFR = descriptionFR;
    }

    public String getDescriptionFR() {
        return descriptionFR;
    }
}
