package com.boncopbadcop.boncopbadcop.Model;

public class CommentModel {
    String feedcomment;
    String username;

    public String getFeedcomment() {
        return feedcomment;
    }

    public void setFeedcomment(String feedcomment) {
        this.feedcomment = feedcomment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
