package com.boncopbadcop.boncopbadcop.Model;

public class ApointmentModel {
    String AppointmentDate;
    String AppointmentPhone;

    public String getAppointmentDate() {
        return AppointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        AppointmentDate = appointmentDate;
    }

    public String getAppointmentPhone() {
        return AppointmentPhone;
    }

    public void setAppointmentPhone(String appointmentPhone) {
        AppointmentPhone = appointmentPhone;
    }
}
