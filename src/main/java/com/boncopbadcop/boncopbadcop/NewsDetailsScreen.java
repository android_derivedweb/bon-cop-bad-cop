package com.boncopbadcop.boncopbadcop;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.boncopbadcop.boncopbadcop.Adapter.AudioAdapterAdapter;
import com.boncopbadcop.boncopbadcop.Adapter.CommentAdapter;
import com.boncopbadcop.boncopbadcop.Model.CommentModel;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class NewsDetailsScreen extends AppCompatActivity {
    private Locale myLocale;
    private KProgressHUD progressDialog;
    private TextView Txt_Name,Txt_Comment_Count,Txt_Date,Txt_Description,Txt_LikeUP,Txt_LikeDown;
    private ImageView img;
    private ArrayList<CommentModel> mCommentArraylist = new ArrayList<>();
    private RecyclerView recyleview;
    private LinearLayoutManager linearlayout;
    private CommentAdapter mAdapter;
    private EditText commnet_type;
    private ImageView comment_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsdetails_screen);
        setLocale(PreferencesUtils.getString(AppConstant.LANGUAGE));

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        final String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("Id");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("Id");
        }
        Txt_Name = findViewById(R.id.name);
        Txt_Date = findViewById(R.id.date);
        Txt_Description = findViewById(R.id.description);
        Txt_LikeUP = findViewById(R.id.likeup);
        Txt_LikeDown = findViewById(R.id.likedown);
        Txt_Comment_Count= findViewById(R.id.comment_count);
        img = (ImageView) findViewById(R.id.images);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyleview = findViewById(R.id.recyleview);
        recyleview.setHasFixedSize(true);
        linearlayout = new LinearLayoutManager(NewsDetailsScreen.this, LinearLayoutManager.VERTICAL, false);
        recyleview.setLayoutManager(linearlayout);
        mCommentArraylist.clear();
        mAdapter = new CommentAdapter(mCommentArraylist, new CommentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        recyleview.setAdapter(mAdapter);

        commnet_type = findViewById(R.id.type_comment);
        comment_send = findViewById(R.id.comment_send);

        comment_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(commnet_type.getText().toString().isEmpty()){
                    Toast.makeText(NewsDetailsScreen.this,"Please enter comment",Toast.LENGTH_SHORT).show();
                }else {
                    AddComment(newString,commnet_type.getText().toString());
                }
            }
        });

        findViewById(R.id.like).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LikeDisLike(newString,"1");
            }
        });

        findViewById(R.id.disslike).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LikeDisLike(newString,"0");
            }
        });
        Log.e("Token",PreferencesUtils.getString(AppConstant.APIToken));
        GetNewsDetails(newString);
    }
    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void GetNewsDetails(String Id){
        progressDialog = KProgressHUD.create(NewsDetailsScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.get(AppConstant.BASE_PATH+"get-feed-by-id?FeedID="+Id)
                .addHeaders("Authorization","Bearer "+PreferencesUtils.getString(AppConstant.APIToken))
                .addHeaders("Accept","application/json")
                .setTag("FeedID_Details")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(NewsDetailsScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {
                                JSONArray object = jsonObject.getJSONArray("data");
                                JSONArray comment = jsonObject.getJSONArray("comment");

                                JSONObject FinalData = object.getJSONObject(0);

                                String FeedID = FinalData.getString("FeedID");
                                String Title = FinalData.getString("Title");
                                String TitleFr = FinalData.getString("TitleFr");
                                String DescriptionFr = FinalData.getString("DescriptionFr");
                                String Description = FinalData.getString("Description");
                                String Image = FinalData.getString("Image");
                                String Date = FinalData.getString("Date");
                                String LikeUp = jsonObject.getString("like_count");
                                String LikeDown = jsonObject.getString("dislike_count");
                                String Comment = jsonObject.getString("comment_count");

                                if(PreferencesUtils.getString(AppConstant.LANGUAGE).equals("en")){
                                    Txt_Name.setText(Title);
                                    Txt_Description.setText(Description);
                                }else {
                                    Txt_Name.setText(""+TitleFr);
                                    Txt_Description.setText(""+DescriptionFr);
                                }


                                Txt_Date.setText(Date);
                                Txt_LikeUP.setText(LikeUp);
                                Txt_LikeDown.setText(LikeDown);
                                Txt_Comment_Count.setText(Comment);
                                Glide.with(NewsDetailsScreen.this).load(Image).placeholder(R.drawable.apk_icon).error(R.drawable.apk_icon).into(img);

                                for(int i = 0 ; i < comment.length() ; i++){
                                    JSONObject object1 = comment.getJSONObject(i);

                                    CommentModel commentModel = new CommentModel();
                                    commentModel.setFeedcomment(object1.getString("FeedComment"));
                                    commentModel.setUsername(object1.getString("Username"));
                                    //commentModel.setUsername("Username");
                                    mCommentArraylist.add(commentModel);
                                }
                                mAdapter.notifyDataSetChanged();
                            }else {
                                Toast.makeText(NewsDetailsScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(NewsDetailsScreen.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(NewsDetailsScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void AddComment(String Id,String comment){
        progressDialog = KProgressHUD.create(NewsDetailsScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"add-comment")
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+PreferencesUtils.getString(AppConstant.APIToken))
                .addBodyParameter("UserID",""+PreferencesUtils.getInt(AppConstant.UserID,0))
                .addBodyParameter("FeedID",Id)
                .addBodyParameter("FeedComment",comment)
                .setTag("FeedID_Details")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        mCommentArraylist.clear();
                        commnet_type.setText("");
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(NewsDetailsScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {
                                JSONArray object = jsonObject.getJSONArray("data");

                                String Comment = jsonObject.getString("comment_count");
                                Txt_Comment_Count.setText(Comment);

                                for(int i = 0 ; i < object.length() ; i++){
                                    JSONObject object1 = object.getJSONObject(i);

                                    CommentModel commentModel = new CommentModel();
                                    commentModel.setFeedcomment(object1.getString("FeedComment"));
                                    commentModel.setUsername(object1.getString("Username"));
                                    mCommentArraylist.add(commentModel);
                                }
                                mAdapter.notifyDataSetChanged();
                            }else {
                                Toast.makeText(NewsDetailsScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(NewsDetailsScreen.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(NewsDetailsScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();}
                });

    }

    public void LikeDisLike(String Id,String FeedLikeDislike){
        progressDialog = KProgressHUD.create(NewsDetailsScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();
        AndroidNetworking.post(AppConstant.BASE_PATH+"add-like-dislike")
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+PreferencesUtils.getString(AppConstant.APIToken))
                .addBodyParameter("UserID",""+PreferencesUtils.getInt(AppConstant.UserID,0))
                .addBodyParameter("FeedID",Id)
                .addBodyParameter("FeedLikeDislike",FeedLikeDislike)
                .setTag("dislike")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        mCommentArraylist.clear();
                        commnet_type.setText("");
                        Log.e("Response : ", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("ResponseCode")==401){
                                PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                                PreferencesUtils.clear();
                                Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(NewsDetailsScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (jsonObject.getInt("ResponseCode")==200) {

                                String LikeUp = jsonObject.getString("like_count");
                                String LikeDown = jsonObject.getString("dislike_count");
                                Txt_LikeUP.setText(LikeUp);
                                Txt_LikeDown.setText(LikeDown);

                            }else {


                                Toast.makeText(NewsDetailsScreen.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(NewsDetailsScreen.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        PreferencesUtils.putBoolean(AppConstant.IsLogin,false);
                        PreferencesUtils.clear();
                        Intent i =  new Intent(NewsDetailsScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        Log.e("Error",anError.getErrorBody());
                        Toast.makeText(NewsDetailsScreen.this,"Unauthenticated",Toast.LENGTH_SHORT).show();
                    }
                });

    }




}